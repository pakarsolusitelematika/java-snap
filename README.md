This project is my personnal processing work, shared publicly to benefit other students and users of the ESA SNAP engine and Sentinel-1 Toolbox. 
For people who aren't familiar with java : the relevent files to see the syntax or structure of the code are in src/main/java/com/batchprocessing/java/snap/

The project focuses on the processing of RADARSAT-2 fully polarimetric images. Processing steps currently covered include :
- subset
- radiometric calibration
- convert to C3 matrix
- speckle filtering
- compute polarimetric parameters (general parameters and HAalpha decomposition)
- orthorectify parameter bands

To do : 
- multiply product with a water mask
- classify the sea ice


I cannot garantee the code is free of errors, and if you discover one please communicate with me.
S. Dufour-Beauséjour (SDB)
INRS-ETE in Québec (QC), Canada
s.dufour.beausejour@gmail.com