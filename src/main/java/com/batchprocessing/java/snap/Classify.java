/*
 * Classify product
 * - maxLikelihood: classify with the maximum likelihood classifier (Product)
 * - KMeans : classify with the K-Means cluster analysis (Product)
 * - addClassMasks : add mask bands for each labled class (Product)
 */
package com.batchprocessing.java.snap;

import java.io.File;
import java.io.FileFilter;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.datamodel.RasterDataNode;
import org.esa.snap.core.gpf.GPF;

/**
 *
 * @author dufobeso
 * S. Dufour-Beauséjour
 * PhD student at INRS-ETE
 * Québec, Canada
 * s.dufour.beausejour@gmail.com
 */
public class Classify {
 
    
    /* Classification with the three ice types */
    public static Product maxLikelihood(Product product, File imageFile, String[] types, String logName){
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();
    
        // Set classification pixel types
        String bayName = Utils.getBay(imageFile);

        // Import training area vectors for this bay     
        FileFilter bayFilter = new RegexFileFilter(bayName+".*shp");
        File[] trainingFiles = new File("Z:\\\\Donnees\\\\Imagerie\\\\VectorData\\\\Training\\\\").listFiles(bayFilter);
        System.out.println("All files and directories:");
        Utils.displayFiles(trainingFiles);
        
        File shapeFile = null;
        String[] trainingVectors = new String[types.length];  
        int n = 0;
        String typesTogether = null;
        for (String type : types) {
            typesTogether += type+"  ";
            // Find vector name matching the ice type
            String vectorName = null;
            for (File trainingFile : trainingFiles) {
                String trainingFileName = trainingFile.getName();
                Pattern typePattern = Pattern.compile("("+bayName+type+".*)\\.shp");
                Matcher vectorMatcher = typePattern.matcher(trainingFileName);
                
                if (vectorMatcher.find()) {
                    vectorName = vectorMatcher.group(1);
                    System.out.println("Matched vectorName : "+vectorName);
                    break;
                } else {
                    continue;
                }              
            }
                   
            shapeFile = new File("Z:\\\\Donnees\\\\Imagerie\\\\VectorData\\\\Training\\\\"+vectorName+".shp");
            System.out.println("ShapeFile name : "+shapeFile.getName());
            if (shapeFile.exists()) {
                product = Utils.addVector(product, imageFile, shapeFile);
                trainingVectors[n] = vectorName+"_1";
                n++;
            }
        }
            
        System.out.println("\nRasterDataNodes are :");
        for (RasterDataNode s : product.getRasterDataNodes()) {
            System.out.print(s.getName()+"  ");
        }

        String[] bands = {"SigmaHH","SigmaHV","Entropy"};
        /* Operator parameters*/
        HashMap parameters = new HashMap();
        parameters.put("trainOnRaster","False");
        parameters.put("trainingVectors",trainingVectors);
        parameters.put("featureBands",bands);
        parameters.put("evaluateClassifier","True");
        parameters.put("evaluateFeaturePowerSet","False");
        parameters.put("savedClassifierName",logName);
        
        System.out.println("Classifying product with MLC...");
        Product outProduct = GPF.createProduct("Maximum-Likelihood-Classifier", parameters, product);        
        System.out.println("Done.");
        
        /* Update product description and name */
        String date = Utils.getTimeStamp();
        
        outProduct.setDescription(product.getDescription()+"Product classified using MLC with training"+ typesTogether +"(" + date + ").\n");
        outProduct.setName(product.getName()+"_mlc");
        return outProduct;   
    }
    
    public static Product KMeans(Product product, File imageFile, int numClass, String[] bands){
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();
        
        /* Operator parameters*/
        HashMap parameters = new HashMap();
        parameters.put("clusterCount",numClass);
        parameters.put("iterationCount",30);
        parameters.put("sourceBandNames",bands);
        
        System.out.println("Classifying product with KMeans cluster analysis...");
        System.out.println("Using the following bands for classification : ");
        for (String s : bands) {
                System.out.print(s+"  ");
        }        
        Product outProduct = GPF.createProduct("KMeansClusterAnalysis", parameters, product);        
        System.out.println("Done.");
        
        /* Update product description and name */
        String date = Utils.getTimeStamp();
        
        outProduct.setDescription(product.getDescription()+"Product classified using K-Means cluster analysis with bands ");
        for (String s : bands) {
            outProduct.setDescription(product.getDescription()+s+" ");
        }    
        outProduct.setDescription(product.getDescription()+" (" + date + ").\n");
        outProduct.setName(product.getName()+"_km");
        return outProduct;           
        
    }
    
    /* Create new mask bands from classes */
    public static Product addClassMasks(Product product, int numClass, String classif) {
        
        System.out.println("Creating virtual band masks for each class, converting them to real bands ...");       
        for (int i = 0; i < numClass; i++){
            
            if (classif.contains("km")){
                product.addBand("mask_"+i, "if class_indices =="+ i +"then 1 else 0"); 
            } else {
                product.addBand("mask_"+i, "if LabeledClasses =="+ i +"then 1 else 0"); 
            }
            
            System.out.println("Product bands are :");
            for (String s : product.getBandNames()) {
                System.out.print(s+"  ");
            }
            
            MyDesktopUtils.convertComputedBandIntoBand(product.getBand("mask_"+i));
        }
        System.out.println("Done.");

        return product;
    } 
}
