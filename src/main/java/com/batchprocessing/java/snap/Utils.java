/*
 * Utilities :
 * - addVector: Add a vector without seperating the shapes (Product) 
 * - createDirectory : Creates a new directory if it doesn't exist (File)
 * - createInSARWorkingDirectory : Creates and returns the workingDirectory for InSAR (File)
 * - createReadme: Creates a readme file with header if it doesn't exist ()
 * - createWorkingDirectory: Creates and returns the workingDirectory (File)
 * - deleteDir: Deletes the directory and all its content ()
 * - deleteSuperfluousVectors : Deletes all water vectors except _1 ()
 * - displayFiles: Print files in file array ()
 * - findMatchingImages: Match string pattern to files in directory and return file array of matches (File[]) 
 * - getBandNames : Returns band names for TSX series (String[])
 * - getBay: Returns the name of the bay based on the date; must be updated with acquisistions (String)
 * - getBayPattern : Returns the pattern of dates matching the input bayName (String)
 * - getDirectory : Returns the directory containing all images from given sensor (File)
 * - getDualType : Returns the string for the dual-pol type : HH/HV, VH/VV, HH/VV from readme (String)
 * - getImageStackName : Returns the image stack name from an array of image files (String)
 * - getImageName : Returns a shorter imageName (String)
 * - getKennaughRGB : Returns a string with the list of Kennaugh elements in RGB order (String)
 * - getProductXML : Returns the TSX product .xml file needed to read the product (File) 
 * - getPolarizationNames : Returns a string array of the name of image polarizations (String[])
 * - getSourceBands : Returns a String with the source band names such as "i_HH,q_HH,i_VV,q_VV" (String)
 * - getTimeStamp : Returns the time yyyy/MM/dd HH:mm:ss (String)
 * - getTSXseriesDirectory : Returns the seriesDirectory for TSX data (String)
 * - getWorkingDirectory : Returns the workingDirectory if it exists (File)
 * - updateReadme : Returns the readme (File)
 * - writeDualType : Writes the dual type from origin product bands to the readme ()
 */
package com.batchprocessing.java.snap;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Scanner;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.gpf.GPF;

/**
 *
 * @author dufobeso
 * S. Dufour-Beauséjour
 * PhD student at INRS-ETE
 * Québec, Canada
 * s.dufour.beausejour@gmail.com
 */
public class Utils {
    /* Import shapefile as vector without seperating shapes */
    public static Product addVector(Product product, File imageFile, File shapeFile) {
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();
                           
        HashMap parameters = new HashMap();
 
        parameters.put("vectorFile", shapeFile);
        parameters.put("seperateShapes","False");
        
        // Imports vector shapefile into product
        Product outProduct = GPF.createProduct("Import-Vector", parameters, product);

        return outProduct;
    }    
    
    public static HashMap addTrainingVectors(Product product, File imageFile, String[] types){
        
        // Import training area vectors for this bay     
        String bayName = getBay(imageFile);

        FileFilter bayFilter = new RegexFileFilter(bayName+".*shp");
        File[] trainingFiles = new File("Z:\\\\Donnees\\\\Imagerie\\\\VectorData\\\\Training\\\\").listFiles(bayFilter);
        System.out.println("All files and directories:");
        Utils.displayFiles(trainingFiles);
        
        File shapeFile = null;
        String[] trainingVectors = new String[types.length];  
        int n = 0;
        String typesTogether = null;
        for (String type : types) {
            typesTogether += type+"  ";
            // Find vector name matching the ice type
            String vectorName = null;
            for (File trainingFile : trainingFiles) {
                String trainingFileName = trainingFile.getName();
                Pattern typePattern = Pattern.compile("("+bayName+type+".*)\\.shp");
                Matcher vectorMatcher = typePattern.matcher(trainingFileName);
                
                if (vectorMatcher.find()) {
                    vectorName = vectorMatcher.group(1);
                    System.out.println("Matched vectorName : "+vectorName);
                    break;
                } else {
                    continue;
                }              
            }
                   
            shapeFile = new File("Z:\\\\Donnees\\\\Imagerie\\\\VectorData\\\\Training\\\\"+vectorName+".shp");
            System.out.println("ShapeFile name : "+shapeFile.getName());
            if (shapeFile.exists()) {
                product = addVector(product, imageFile, shapeFile);
                trainingVectors[n] = vectorName+"_1";
                n++;
            }
        }
        HashMap map = new HashMap();
        map.put("product", product);
        map.put("vectorNames",trainingVectors);
               
        return map;
    }
    
    /* Create directory if it doesn't exist*/
    public static File createDirectory(File imageFile, String directoryName){
        
        /* From shortened image name, create working directory if not already created */
        String imageName = getImageName(imageFile);
        
        File newDirectory = new File(imageFile.getParent(),directoryName);
                
        if (!(newDirectory.isDirectory())){
            newDirectory.mkdir();
            System.out.println("New directory \"" + newDirectory.getPath()+"\" created.");
        }
        return newDirectory;
    }
    
    /* Create the InSAR working directory if it doesn't exist*/
    public static File createInSARWorkingDirectory(File[] imageFiles){
        /* Fetch image stack name */
        String imageStackName = getImageStackName(imageFiles);
        
        /* From image stack name, create working directory if not already created */        
        File workingDirectory = new File(imageFiles[0].getParent(),"InSAR\\\\"+imageStackName);
                
        if (!(workingDirectory.isDirectory())){
            workingDirectory.mkdir();
            System.out.println("Working directory \"" + workingDirectory.getPath()+"\" created.");
        }
        return workingDirectory;
    }    
    
    /* Create readme.txt file in working directory if it doesn't already exist. Returns a readme File. */
    public static void createReadme(File workingDirectory) throws IOException {
        
        File readmeFile = new File(workingDirectory,"readme.txt");
        
        if (!(readmeFile.exists())){
            try (BufferedWriter readme = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(readmeFile),StandardCharsets.UTF_8))){
                readme.write("S. Dufour-Beauséjour \n"
                + "s.dufour.beausejour@gmail.com \n"
                + "Doctorante en sciences de l'eau \n"
                + "INRS-ETE \n"
                + "Québec \n\n"
                + "Image analysis done in SNAP via java \n\n"
                + "This folder contains all the image analysis done on the associated image or image stack. \n \n"
                + "Analysis steps : \n");
            }
            System.out.println("Created a readme.txt file.");
        }              
    }
    
    /* Create the working directory if it doesn't exist*/
    public static File createWorkingDirectory(File imageFile){
        /* From shortened image name, create working directory if not already created */
        String imageName = getImageName(imageFile);
        
        File workingDirectory = new File(imageFile.getParent(),imageName);
                
        if (!(workingDirectory.isDirectory())){
            workingDirectory.mkdir();
            System.out.println("Working directory \"" + workingDirectory.getPath()+"\" created.");
        }
        return workingDirectory;
    }
    
    /* Delete folder */
    public static void deleteDir(File file) {
        File[] contents = file.listFiles();
        if (contents != null) {
            for (File f : contents){
                deleteDir(f);
            }
        }
        file.delete();
    }
    
    /* Delete unwanted vectors */
    public static void deleteSuperfluousVectors(File imageFile, String processingDone) {
        String imageName = getImageName(imageFile);        
        File workingDirectory = new File(imageFile.getParent(),imageName);
        // Fetch vector list               
        File directory = new File(workingDirectory, imageName + processingDone +".data\\\\vector_data\\\\");
        File[] files = directory.listFiles();

        String pattern = ("(Water|mask).*csv");
        FileFilter filter = new RegexFileFilter(pattern);
        
        files = directory.listFiles(filter);        
        for (File f : files){
            f.delete();
        }
    }
    
    /* Display (print) all files in directory*/
    public static void displayFiles(File[] files) {
        for (File file : files) {
            System.out.println(file.getName());
        }
    }
    
    
    /* Find matching images */
    public static File[] findMatchingImages(File directory, String pattern) throws IOException {

        File[] files = directory.listFiles();
        //System.out.println("All files and directories:");
        //displayFiles(files);

        //String pattern = "[tT]est[1-3].txt";
        System.out.println("\nFiles that match regular expression: " + pattern);
        FileFilter filter = new RegexFileFilter(pattern);
        files = directory.listFiles(filter);
        
        Arrays.sort(files);
        displayFiles(files);
        return files;
    }    
    
    /* Get band names for TSX image series */
    public static String[] getBandNames(String seriesName){
        String[] bandNames = null;
        if (seriesName.equals("DeceptionBay_orbit21_IncAngle40_Processed")) {
            bandNames = new String[] {"K0","K1","K5","K8"};
        } else {
            System.out.println("Unexpected seriesName in Utils.getBandNames");
        }
        return bandNames;
    }
    
    /* Get bay name */
    public static String getBay(File imageFile){
        
        /* Correspondance between acquisition dates and bays; will have to be updated with every acquisition. */
        String patternSalluit = getBayPattern("Salluit");
        String patternKangiqsujuaq = getBayPattern("Kangiqsujuaq");
        String patternDeceptionBay = getBayPattern("DeceptionBay");
                       
        String imageName = Utils.getImageName(imageFile);

        String bayName = new String();
        /* Try matching imageName to expected dates and thus identify the bay. */
        if (imageName.matches(patternSalluit.substring(0, patternSalluit.length()-11))){
            bayName = "Salluit";
        }
        else if (imageName.matches(patternKangiqsujuaq.substring(0,patternKangiqsujuaq.length()-11))){
            bayName = "Kangiqsujuaq";
        }
        else if (imageName.matches(patternDeceptionBay.substring(0,patternDeceptionBay.length()-11))){
            bayName = "DeceptionBay";
        }
        else {
            System.out.println("Could not match imageName to expected date and thus could not identify the bay.");
            // This should cause the processing to stop, but for now it doesn't.
        }
                
        /* Return geometry string */
        return bayName;
    }   
    
    public static String getBayPattern(String bayName) {
        String pattern = null;
        if (bayName.contains("Salluit")){
            pattern = (".*(20151219|(2016(0112|0205|0229|0324|0417|1213))|(2017(0106|0130|0223|0319|0412))).*(SLC|SLC_1)");
        } else if (bayName.contains("Kangiqsujuaq")){
            pattern = (".*(20151223|(2016(0116|0209|0304|0328|0421|1217))|(2017(0110|0203|0227|0323|0416))).*(SLC|SLC_1)");
        } else if (bayName.contains("DeceptionBay")){
            pattern = (".*(20151226|(2016(0119|0212|0307|0331|0424|1220))|(2017(0113|0206|0302|0326|0419))).*(SLC|SLC_1)");
        }
        return pattern;
    }
    
    /* Get the directory containing all images (either RS2 or TSX) */
    public static File getDirectory(String sensor){
        File directory = null;
        if (sensor.contains("RADARSAT-2")){
            directory = new File("Z:\\\\Donnees\\\\Imagerie\\\\RADARSAT-2\\\\");
        } else if (sensor.contains("TerraSAR-X")){
            directory = new File("Z:\\\\Donnees\\\\Imagerie\\\\TerraSAR-X\\\\");
        } else if (sensor.contains("MacRS2")){
            directory = new File("C:\\\\Users\\\\dufobeso\\\\Desktop\\\\RS2_Analysis\\\\Images");
        } else if (sensor.contains("ExternalDriveTSX")){
            directory = new File("F:\\\\Doctorat\\\\BaieDeception\\\\Donnees\\\\Imagerie\\\\TerraSAR-X\\\\");
        }
        return directory;
    }
    
    /* Get the type of dual pol : VH/VV, HH/HV... from readme */
    public static String getDualType(File imageFile) throws IOException{
        String dualType = null;
        File workingDirectory = Utils.getWorkingDirectory(imageFile);        
        File readmeFile = Utils.updateReadme(workingDirectory);

        Scanner scanner = new Scanner(readmeFile);
        
        boolean foundDualType = false;
        int lineNum = 0;
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            lineNum++;
            if (line.contains("Dual type")) {
                dualType = line.substring(15, 21);
                System.out.println("Dual type is : " + dualType);
                foundDualType = true;
                break;
            }
        }
        if (!foundDualType){
            System.out.println("Failed to find dualType in the readme file.");
        }
        return dualType;
    }
    
    /* Get a shorter image name from the imageFile */
    public static String getImageName(File imageFile){
        
        /* From imageFile, get shorter imageName ex. RS2_20151219 */
        String longImageName = new String(imageFile.getName());
        String imageName = null;
        if (imageFile.toString().contains("RS2")) {
            Pattern datePattern = Pattern.compile(".*(\\d{8}+)_.*(SLC|SLC_1)");
            Matcher dateMatcher = datePattern.matcher(longImageName);

            if (dateMatcher.find()) {
                imageName = "RS2_" + dateMatcher.group(1);
            } else {
                imageName = "Could not find match between image name and regex.";
            }
        } else if (imageFile.toString().contains("TSX1_SAR")) {
            Pattern datePattern = Pattern.compile("TSX.*SRA_(\\d{8}+).*");
            Matcher dateMatcher = datePattern.matcher(longImageName);

            if (dateMatcher.find()) {
                imageName = "TSX_" + dateMatcher.group(1);
            } else {
                imageName = "Could not find match between image name and regex.";
            }
        } else if (imageFile.toString().contains("TSX_db")) {
            Pattern datePattern = Pattern.compile(".*_(\\d{8}+).*TSX_db.tif");
            Matcher dateMatcher = datePattern.matcher(longImageName);

            if (dateMatcher.find()) {
                imageName = "TSX_" + dateMatcher.group(1);
            } else {
                imageName = "Could not find match between image name and regex.";
            }
        } else {
            imageName = "Could not identify sensor in Utils.getImageName";
        }
        return imageName;
    }
        
    /* Return the image stack name for InSAR   */
    public static String getImageStackName(File[] imageFiles){
        String imageStackName = Utils.getImageName(imageFiles[0]).substring(0, 3);
        for (File imageFile : imageFiles){
            imageStackName += "_" + Utils.getImageName(imageFile).substring(4);
        }
        
        return imageStackName;
    }
    
    /* Get quickfile for Kennaugh RGB composite */
    public static String getKennaughRGB(String seriesName){
        String KennaughRGB = null;
        if (seriesName.equals("DeceptionBay_orbit21_IncAngle40_Processed")) {         
            KennaughRGB = "K1_K0_K5";
        } else {
            System.out.println("Unexpected seriesName in Utils.getKennaughRGB.");
        }
        return KennaughRGB;
    }    
    
    /* Get product xml file */
    public static File getProductXML(File imageFile) throws IOException{

        System.out.print("Retrieving product xml...");
        File[] files = Utils.findMatchingImages(imageFile, ".*.xml");
        
        return files[0];
    }    
    
    /* Return the image polarizations (HH,HV,VH,VV) as a String array */
    public static String getPolarizationNames(Product product){
        String[] bandNames = product.getBandNames();
        
        String polarizationNames = "";
        for (String bandName : bandNames){
            if ((bandName.contains("Intensity"))){
                polarizationNames += bandName.substring(10);
                polarizationNames += ",";
            }
        }
        
        return polarizationNames.substring(0, 5);
    }
    
    /* Return the source bands of polarimetric product as a String */
    public static String getSourceBands(Product product){
        String[] bandNames = product.getBandNames();
        String sourceBands = "";
        for (String bandName : bandNames){
            if (!(bandName.contains("Intensity"))){
                sourceBands += bandName;
                sourceBands += ",";
            }
        }
        return sourceBands;
    }
    

    /* Return time stamp to be used in readme update */
    public static String getTimeStamp(){
        DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date today = Calendar.getInstance().getTime();
        String timeStamp = df.format(today);   
        return timeStamp;
    }
    
    /* TSX data is organized by bay and orbit */
    public static String getTSXseriesDirectory(String seriesName){
        String seriesDirectory = null;
        if (seriesName.equals("DeceptionBay_orbit21_IncAngle40_Processed")){
            seriesDirectory = "DeceptionBay_orbit21_IncAngle40_Processed\\\\";
        } else if (seriesName.contains("DeceptionBay_orbit21")){
            seriesDirectory = "DeceptionBay_orbit21_IncAngle40\\\\";
        } else if (seriesName.contains("DeceptionBay_orbit89")){
            seriesDirectory = "DeceptionBay_orbit89_IncAngle46\\\\";
        } 
        return seriesDirectory;
    }    
    
    /* Get the working directory*/
    public static File getWorkingDirectory(File imageFile){
        
        /* From shortened image name, create working directory if not already created */
        String imageName = getImageName(imageFile);
        
        File workingDirectory = new File(imageFile.getParent(),imageName);
                
        if (!(workingDirectory.isDirectory())){
            System.out.println("Working directory \"" + workingDirectory.getPath()+"\" doesn't exist.");
        }
        return workingDirectory;
    }    
       
    /* Returns readme.txt file and warns if it hasn't been created yet. */
    public static File updateReadme(File workingDirectory) throws IOException {
        
        File readmeFile = new File(workingDirectory,"readme.txt");
        
        if (!(readmeFile.exists())){
            System.out.println("readme.txt does not exist.");
        }        
        return readmeFile;
    }    
    
    /* Get the type of dual pol : VH/VV, etc from original product and write dualType to readme file*/
    public static void writeDualType(Product product, File imageFile) throws IOException {
        String dualType = null;
        Product originalProduct = ReadProduct.TSX(imageFile);
        String sourceBands = Utils.getSourceBands(originalProduct);
        if (sourceBands.contains("HH") && sourceBands.contains("HV")) {
            dualType = "HH/HV";
        } else if (sourceBands.contains("VH") && sourceBands.contains("VV")) {
            dualType = "VH/VV";
        } else if (sourceBands.contains("HH") && sourceBands.contains("VV")) {
            dualType = "HH/VV";
        } else {
            System.out.println("Unexpected sourceBands.");
        }
        
        File workingDirectory = Utils.getWorkingDirectory(imageFile);        
        File readmeFile = Utils.updateReadme(workingDirectory);
        String date = Utils.getTimeStamp();
        
        try (BufferedWriter readme = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(readmeFile, true), StandardCharsets.UTF_8))) {
                readme.append("Dual type is : " + dualType + "  (" + date + ")\n");
        }
        
    }    
}
