/*
 * Get the intersection of the product and a water mask
 * also multiply by a polygon created to crop the water area to the region of interest
 * - addROI: import ROI vector into product (Product)
 * - addWater: import water vector into product (Product)
 * - getIntersection: create mask from water vector, 
 *      create and write the intersection of the product with this mask (Product)
 * - getIntersection2: create masks from ROI and water vectors, 
 *      create and write the intersection of the product with these masks (Product)
 */
package com.batchprocessing.java.snap;

import java.awt.Color;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import org.esa.snap.core.dataio.ProductIO;
import org.esa.snap.core.dataio.ProductWriter;
import org.esa.snap.core.datamodel.Band;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.datamodel.ProductNode;
import org.esa.snap.core.datamodel.ProductNodeGroup;
import org.esa.snap.core.datamodel.VectorDataNode;
import org.esa.snap.core.gpf.GPF;
import org.esa.snap.core.datamodel.Mask;
import org.esa.snap.core.datamodel.ProductData;
import org.esa.snap.core.datamodel.RasterDataNode;
import org.esa.snap.core.util.ProductUtils;

/**
 *
 * @author dufobeso
 * S. Dufour-Beauséjour
 * PhD student at INRS-ETE
 * Québec, Canada
 * s.dufour.beausejour@gmail.com
 */
public class Masking {
    
    /* Add the ROI mask raster to the product as a vector */
    public static Product addROI(Product product, File imageFile) {
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();
                       
        // Mask shapefile
        String bayName = Utils.getBay(imageFile);
        File ROIFile = null;
        
        if (bayName == "Salluit"){           
            ROIFile = new File("Z:\\\\Donnees\\\\Imagerie\\\\VectorData\\\\ROI\\\\maskSalluit_strait_MultiPolygon.shp");
            System.out.println("Bay name is "+bayName);
        } else if (bayName == "Kangiqsujuaq"){
            ROIFile = new File("Z:\\\\Donnees\\\\Imagerie\\\\VectorData\\\\ROI\\\\maskKangiqsujuaq_strait_MultiPolygon.shp");
            System.out.println("Bay name is "+bayName);
        } else if (bayName == "DeceptionBay"){
            ROIFile = new File("Z:\\\\Donnees\\\\Imagerie\\\\VectorData\\\\ROI\\\\maskDeceptionBay_Polygon.shp");
            System.out.println("Bay name is "+bayName);
        }
        
        HashMap parameters = new HashMap();
 
        parameters.put("vectorFile", ROIFile);
        parameters.put("seperateShapes","True");
        
        // Imports vector shapefile into product
        Product outProduct = GPF.createProduct("Import-Vector", parameters, product);

        return outProduct;
    }    
    
    /* Add the water mask raster to the product as a vector */
    public static Product addWater(Product product, File imageFile) {
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();
      
        // Mask shapefile
        File waterFile = new File("Z:\\\\Donnees\\\\Imagerie\\\\VectorData\\\\Water_HudsonBay.shp");
        HashMap parameters = new HashMap();
 
        parameters.put("vectorFile", waterFile);
        parameters.put("seperateShapes","True");
        
        // Imports vector shapefile into product
        Product outProduct = GPF.createProduct("Import-Vector", parameters, product);

        return outProduct;
    }
    
    /* Get the intersection of the mask and the product by creating a new product and manually adding the bands */
    public static Product getIntersection(Product product, File imageFile, String processingDone) throws IOException{
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();

        // Get array of bandNames before adding the mask band
        String[] bandNames = product.getBandNames();
        
        // Fetch vector to use as mask (vector has already been imported into product)
        ProductNodeGroup vectorDataGroup = product.getVectorDataGroup();
        ProductNode waterVector = vectorDataGroup.get("Water_HudsonBay_1");
        System.out.println(waterVector.getName());
        
        // Add mask to product from vector
        Mask waterMask = product.addMask(waterVector.getDisplayName(), (VectorDataNode) waterVector, "HudsonBay water mask", Color.blue, 0);
        
        ////////// New product
        // Initialize new product
        System.out.println("Creating new product for intersection of mask and bands...");
        System.out.println("Product name : " + product.getName()+"_msk");
        System.out.println("Product type : " + product.getProductType());
        System.out.println("Scene raster width : " + product.getSceneRasterWidth()+"; mask :" +waterMask.getRasterWidth());
        System.out.println("Scene raster height : " + product.getSceneRasterHeight()+"; mask :"+waterMask.getRasterHeight());
        
        Product outProduct = new Product(product.getName()+"_msk", product.getProductType(), product.getSceneRasterWidth(), product.getSceneRasterHeight());
        String date = Utils.getTimeStamp();
        outProduct.setDescription(product.getDescription()+"Product multiplied by water mask (" + date + ").\n");
        //ProductUtils.copyGeoCoding(product, outProduct);
        //ProductUtils.copyMetadata(product, outProduct);
        ProductUtils.copyProductNodes(product, outProduct);
        
        // Set product writer and write header 
        ProductWriter writer = ProductIO.getProductWriter("BEAM-DIMAP");
        outProduct.setProductWriter(writer);
        
        // Utils
        File workingDirectory = Utils.createWorkingDirectory(imageFile);
        String imageName = Utils.getImageName(imageFile);
        File outFile = new File(workingDirectory, imageName+processingDone+"_msk.dim");
        
        // Initialize all bands so that they are written to the header, then write the header
        //Band waterMaskBand = outProduct.addBand("WaterMaskBand", ProductData.TYPE_FLOAT32);
        Band[] targetBands = new Band[bandNames.length];
        int k = 0;
        for (String bandName : bandNames) {
            targetBands[k] = outProduct.addBand(bandName, ProductData.TYPE_FLOAT32);
            targetBands[k].setNoDataValueUsed(true);           
            k++;
        }
        
        outProduct.writeHeader(outFile);
        
        
        // Initialize data buffer which will be used for each scan line of band to be written; create divideBy float for normalization
        float[] waterMaskDataBuffer = new float[waterMask.getRasterWidth()];        
        float divideBy = (float) 255;
        
        /* Writing the bands
        Scan each line of the mask to data buffer, then scan each element and normalize to 0 or 1, 
        multiply the mask element with the source band element and then write each line to the target band. 
        java arrays = HxW and getPixels operator is HxW */
        k = 0;
        float[] bandDataBuffer = new float[waterMask.getRasterWidth()];        
        for (String bandName : bandNames) {
            Band sourceBand = product.getBand(bandName);
            Band targetBand = targetBands[k];
            
            for (int i = 0; i < waterMask.getRasterHeight(); i++) {
                waterMaskDataBuffer = (waterMask.readPixels(0, i, waterMask.getRasterWidth(), 1, waterMaskDataBuffer)); // values are 0.0 or 255.0
                bandDataBuffer = (sourceBand.readPixels(0, i, sourceBand.getRasterWidth(), 1, bandDataBuffer));
                
                for (int j = 0; j < waterMask.getRasterWidth(); j++) {
                    waterMaskDataBuffer[j] = waterMaskDataBuffer[j] / divideBy;
                    bandDataBuffer[j] = bandDataBuffer[j]*waterMaskDataBuffer[j];
                }
                
               
                targetBand.writePixels(0, i, waterMask.getRasterWidth(), 1, bandDataBuffer);
            }
            k++;
            //System.out.println(Arrays.toString(bandDataBuffer));
            //System.out.println(Arrays.toString(waterMaskDataBuffer));  
        }
        product.closeIO();
        System.out.println("Done.");
        
        /* Update readme.txt */
        File readmeFile = Utils.updateReadme(workingDirectory);
        //String date = Utils.getTimeStamp();

        try (BufferedWriter readme = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(readmeFile, true), StandardCharsets.UTF_8))) {
            readme.append("Wrote product " + outFile.getName() + "  (" + date + ")\n");
        }    
        
        System.out.println("Product bands are :");
        for (String s : outProduct.getBandNames()) {
            System.out.println(s);
        }

        return outProduct;
    }
    
    /* Get the intersection of two masks and the product by creating a new product and manually adding the bands */
    public static Product getIntersection2(Product product, File imageFile, String processingDone) throws IOException{
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();

        // Get array of bandNames before adding the mask band
        String[] bandNames = product.getBandNames();
                System.out.println("Product raster data nodes are :");
        
        
        for (RasterDataNode s : product.getRasterDataNodes()) {
            System.out.println(s);
        }
        // Fetch vectors to use as masks (vectors have already been imported into product)
        ProductNodeGroup vectorDataGroup = product.getVectorDataGroup();
        ProductNode waterVector = vectorDataGroup.get("Water_HudsonBay_1");
        
        String bayName = Utils.getBay(imageFile);
        System.out.println("bayName is : "+bayName);
        ProductNode ROIVector = null;

        if (bayName == "Salluit"){           
            ROIVector = vectorDataGroup.get("maskSalluit_strait_MultiPolygon_1");
        } else if (bayName == "Kangiqsujuaq"){
            ROIVector = vectorDataGroup.get("maskKangiqsujuaq_strait_MultiPolygon_1");
        } else if (bayName == "DeceptionBay"){
            ROIVector = vectorDataGroup.get("maskDeceptionBay_Polygon_1");
        }
        
        
        System.out.println("First mask : "+waterVector.getName());
        System.out.println("Second mask : "+ROIVector.getName());
       
        // Add mask to product from vector
        Mask waterMask = product.addMask(waterVector.getDisplayName(), (VectorDataNode) waterVector, "HudsonBay water mask", Color.blue, 0);
        Mask ROIMask = product.addMask(ROIVector.getDisplayName(), (VectorDataNode) ROIVector, "ROI mask", Color.green, 0);
        
        ////////// New product
        // Initialize new product
        System.out.println("Creating new product for intersection of masks and bands...");
        System.out.println("Product name : " + product.getName()+"_msk");
        System.out.println("Product type : " + product.getProductType());
        System.out.println("Scene raster width : " + product.getSceneRasterWidth()+"; mask :" +waterMask.getRasterWidth());
        System.out.println("Scene raster height : " + product.getSceneRasterHeight()+"; mask :"+waterMask.getRasterHeight());
        
        Product outProduct = new Product(product.getName()+"_msk", product.getProductType(), product.getSceneRasterWidth(), product.getSceneRasterHeight());
        String date = Utils.getTimeStamp();
        outProduct.setDescription(product.getDescription()+"Product multiplied by water mask and custom mask (" + date + ").\n");
        //ProductUtils.copyGeoCoding(product, outProduct);
        //ProductUtils.copyMetadata(product, outProduct);
        ProductUtils.copyProductNodes(product, outProduct);
        
        // Set product writer and write header 
        ProductWriter writer = ProductIO.getProductWriter("BEAM-DIMAP");
        outProduct.setProductWriter(writer);
        
        // Utils
        File workingDirectory = Utils.createWorkingDirectory(imageFile);
        String imageName = Utils.getImageName(imageFile);
        File outFile = new File(workingDirectory, imageName+processingDone+"_msk.dim");
        
        // Initialize all bands so that they are written to the header, then write the header
        //Band waterMaskBand = outProduct.addBand("WaterMaskBand", ProductData.TYPE_FLOAT32);
        Band[] targetBands = new Band[bandNames.length];
        int k = 0;
        for (String bandName : bandNames) {
            targetBands[k] = outProduct.addBand(bandName, ProductData.TYPE_FLOAT32);
            targetBands[k].setNoDataValueUsed(true);           
            k++;
        }
        
        outProduct.writeHeader(outFile);
        
        
        // Initialize data buffer which will be used for each scan line of band to be written; create divideBy float for normalization
        float[] waterMaskDataBuffer = new float[waterMask.getRasterWidth()];        
        float[] ROIMaskDataBuffer = new float[ROIMask.getRasterWidth()];        
        float divideBy = (float) 255;
        
        /* Writing the bands
        Scan each line of the masks to data buffer, then scan each element and normalize to 0 or 1, 
        multiply the mask elements with the source band element and then write each line to the target band. 
        java arrays = HxW and getPixels operator is HxW */
        k = 0;
        float[] bandDataBuffer = new float[waterMask.getRasterWidth()];        
        for (String bandName : bandNames) {
            Band sourceBand = product.getBand(bandName);
            Band targetBand = targetBands[k];
            
            for (int i = 0; i < waterMask.getRasterHeight(); i++) {
                waterMaskDataBuffer = (waterMask.readPixels(0, i, waterMask.getRasterWidth(), 1, waterMaskDataBuffer)); // values are 0.0 or 255.0
                ROIMaskDataBuffer = (ROIMask.readPixels(0, i, ROIMask.getRasterWidth(), 1, ROIMaskDataBuffer)); // values are 0.0 or 255.0
                bandDataBuffer = (sourceBand.readPixels(0, i, sourceBand.getRasterWidth(), 1, bandDataBuffer));
                
                for (int j = 0; j < waterMask.getRasterWidth(); j++) {
                    waterMaskDataBuffer[j] = waterMaskDataBuffer[j] / divideBy;
                    ROIMaskDataBuffer[j] = ROIMaskDataBuffer[j] / divideBy;
                    bandDataBuffer[j] = bandDataBuffer[j]*waterMaskDataBuffer[j]*ROIMaskDataBuffer[j];
                }
                
               
                targetBand.writePixels(0, i, waterMask.getRasterWidth(), 1, bandDataBuffer);
            }
            k++;
            //System.out.println(Arrays.toString(bandDataBuffer));
            //System.out.println(Arrays.toString(waterMaskDataBuffer));  
        }
        product.closeIO();
        System.out.println("Done.");
        
        /* Update readme.txt */
        File readmeFile = Utils.updateReadme(workingDirectory);
        //String date = Utils.getTimeStamp();

        try (BufferedWriter readme = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(readmeFile, true), StandardCharsets.UTF_8))) {
            readme.append("Wrote product " + outFile.getName() + "  (" + date + ")\n");
        }    
        
        System.out.println("Product bands are :");
        for (String s : outProduct.getBandNames()) {
            System.out.println(s);
        }

        return outProduct;
    }  
}