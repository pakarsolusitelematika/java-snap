/**
 * @author dufobeso
 * S. Dufour-Beauséjour
 * PhD student at INRS-ETE
 * Québec, Canada
 * s.dufour.beausejour@gmail.com
 * 
 * TSX dual-pol or RS2 quad-pol image processing with SNAP through Java
 * All processing can be launched from this Main class by using outerArgs 
 * as one would use args while executing the compiled ProcessTSX.jar or ProcessRS2.jar files. 
 * 
 */

package com.batchprocessing.java.snap;

import com.vividsolutions.jts.io.ParseException;
import java.io.File;
import java.io.IOException;

public class Main {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    
    public static void ProcessRS2() throws IOException, ParseException{
        /* Indicate sensor */
        String sensor = new String("RADARSAT-2");
        //String pattern = ("TSX1.*(2008).*");
        String pattern = (".*(2017).*(SLC|SLC_1)");
                
        /* Look for images matching regex in image directory : */
        File directory = Utils.getDirectory(sensor);
        File[] imageFiles = Utils.findMatchingImages(directory, pattern);
        
        for (File imageFile : imageFiles) {
            String[] outerArgs = new String[] {imageFile.toString()};
            ProcessRS2.main(outerArgs);
        }
    }
    
    public static void ProcessTSX(String seriesName) throws IOException, ParseException{
        /* Indicate sensor */
        String sensor = new String("TerraSAR-X");
        //String sensor = new String("ExternalDriveTSX");
        /* Look for images matching regex in image directory : */
        File globalDirectory = Utils.getDirectory(sensor);
        String seriesDirectory = Utils.getTSXseriesDirectory(seriesName);
        File directory = new File(globalDirectory,seriesDirectory);
        String pattern = new String();        
        if (seriesName.contains("Processed")){
            pattern = (".*(2015|2016|2017).*TSX_db.tif");
        } else {
            pattern = ("TSX1.*(2016|2017).*");
        }
        File[] imageFiles = Utils.findMatchingImages(directory, pattern);
        
        for (File imageFile : imageFiles) {
            String[] outerArgs = new String[] {imageFile.toString(),seriesName};
            if (seriesName.contains("Processed")){               
                ProcessTSX.DLR(outerArgs); 
            } else {
                ProcessTSX.main(outerArgs);                 
            }
        }
    }
    
    public static void ProcessInSAR() throws IOException, ParseException{
        /* Indicate sensor */
        //String sensor = new String("TerraSAR-X");
        String sensor = new String("RADARSAT-2");
        
        /* Look for images matching regex in image directory : */
        File directory = Utils.getDirectory(sensor);
        //String pattern = ("TSX1.*(2008).*");
        String pattern = (".*((20161213)|(20170106)).*(SLC|SLC_1)");
        
        File[] imageFiles = Utils.findMatchingImages(directory, pattern);

        ProcessInSAR.main(imageFiles);
        }
    
    public static void main(String[] args) throws IOException, ParseException {
        ProcessTSX("DeceptionBay_orbit21_IncAngle40_Processed");
        //ProcessRS2();
        //ProcessInSAR();
    }
}
