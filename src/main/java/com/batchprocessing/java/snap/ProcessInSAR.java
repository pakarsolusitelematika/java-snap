/**
 * RS2 quad-pol InSAR with SNAP through Java
 * Processing steps already coded :
 * - Read RS2 BEAM-DIMAP product 
 * - Create working directory
 * - Subset the products : choose only HH band
 * - Create stack of products
 * - Choose GCPs using cross-correlation
 * 
 * TO DO :
 * - coregistration
 * - create interferogram and coherence
 * - remove topography
 * - etc
 *
 */
package com.batchprocessing.java.snap;

import com.vividsolutions.jts.io.ParseException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import org.esa.snap.core.dataio.ProductIO;
import org.esa.snap.core.datamodel.Band;
import org.esa.snap.core.datamodel.Product;

/**
 *
 * @author dufobeso
 * S. Dufour-Beauséjour
 * PhD student at INRS-ETE
 * Québec, Canada
 * s.dufour.beausejour@gmail.com
 */

public class ProcessInSAR {
    
    public static void main(File[] imageFiles) throws IOException, ParseException {
 
        System.out.println("Image stack name : " + Utils.getImageStackName(imageFiles));

        /* Create working directory if it doesn't exist */
        File workingDirectory = Utils.createInSARWorkingDirectory(imageFiles);
        System.out.println("Created working directory : "+workingDirectory.toString());
        /* Create a readme.txt file if it doesn't exist */
        Utils.createReadme(workingDirectory);

        /* Read the BEAM-DIMAP products */
        System.out.println("Reading all products...");
        String processingDone = new String();
        Product[] imageProducts = new Product[imageFiles.length];
        int counter = 0;
        for (File imageFile : imageFiles){    
            imageProducts[counter] = ReadProduct.BEAMDIMAP(imageFile, processingDone);
            counter += 1;
        }
        
        /* Create subset of products - keep HH only*/
        Subset.keepHH(imageProducts);
        processingDone += "_HH";
        
        /* Coregistration : create stack */
        File stackFile = new File(workingDirectory, Utils.getImageStackName(imageFiles) + processingDone + "_stack.dim");
        if (!(stackFile.exists())) {
            Product stackProduct = Coregistration.createStack(imageProducts, imageFiles);
            WriteProduct.BEAMDIMAP(stackProduct, workingDirectory, stackFile);
        }
        processingDone += "_stack";
        
        /* Coregistration : crossCorrelation GCPs */
        File crossFile = new File(workingDirectory, Utils.getImageStackName(imageFiles) + processingDone + "_cross.dim");
        if (!(crossFile.exists())) {           
            Product product = ProductIO.readProduct(stackFile); 
            
            HashMap parameters = new HashMap();
            parameters.put("numGCPtoGenerate","200");

            // Add relevent parameters to readme
            File readmeFile = Utils.updateReadme(workingDirectory);
            try (BufferedWriter readme = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(readmeFile, true), StandardCharsets.UTF_8))) {
                readme.append("\n- numGCPtoGenerate = "+ parameters.get("numGCPtoGenerate") +"\n");
            }
            
            Product crossProduct = Coregistration.crossCorrelation(product, parameters);
            WriteProduct.BEAMDIMAP(crossProduct, workingDirectory, crossFile);           
        }
        processingDone += "_cross";
        
        /* Coregistration : compute warp function */
        File warpFile = new File(workingDirectory, Utils.getImageStackName(imageFiles) + processingDone + "_warp.dim");
        if (!(warpFile.exists())) {
            Product product = ProductIO.readProduct(crossFile);     
            
            HashMap parameters = new HashMap();
            parameters.put("rmsThreshold","0.05");
            parameters.put("warpPolynomialOrder","2");
        
            // Add relevent parameters to readme
            File readmeFile = Utils.updateReadme(workingDirectory);
            try (BufferedWriter readme = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(readmeFile, true), StandardCharsets.UTF_8))) {
                readme.append("- rmsThreshold = "+ parameters.get("rmsThreshold") +"\n");
            }
            
            Product warpProduct = Coregistration.computeWarp(product, parameters);
            WriteProduct.BEAMDIMAP(warpProduct, workingDirectory, warpFile);          
        }
        processingDone += "_warp";
    }      
}