/*
 * Flip the product either :
 * - vertically (Product)
 * - horizontally (Product)
 */
package com.batchprocessing.java.snap;

import java.io.File;
import java.util.HashMap;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.datamodel.ProductData;
import org.esa.snap.core.gpf.GPF;

/**
 *
 * @author S. Dufour-Beauséjour (s.dufour.beausejour@gmail.com)
 */
public class Flip {

    public static Product vertically(Product product, File imageFile) {
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();
        
        /* Operator parameters */
        HashMap parameters = new HashMap();
        parameters.put("flipType", "Vertical");
        
        System.out.println("Flipping product vertically...");
        Product outProduct = GPF.createProduct("Flip", parameters, product);
        System.out.println("Done.");

        /* Add Intensity virtual bands */
        String[] bandNames = outProduct.getBandNames();
        for (String bandName : bandNames){
            System.out.println(bandName);
            if(bandName.contains("i_")){
                String polarization = bandName.substring(2);
                // remove band, replace with virtual band
                outProduct.removeBand(outProduct.getBand("Intensity_"+polarization));                               
                outProduct.addBand("Intensity_"+polarization, "i_"+polarization+" * i_"+polarization+" + q_"+polarization+" * q_"+polarization+"", ProductData.TYPE_FLOAT32);               
                System.out.println("Added virtual band : Intensity_"+polarization);
            }
        }      
              
        /* Update product description and name */
        String date = Utils.getTimeStamp();

        outProduct.setDescription(product.getDescription() + "Product flipped vertically (" + date + ").\n");
        outProduct.setName(product.getName() + "_Vflip");
        return outProduct;
    }
    
    public static Product horizontally(Product product, File imageFile) {
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();

        /* Operator parameters */
        HashMap parameters = new HashMap();
        parameters.put("flipType", "Horizontal");

        System.out.println("Flipping product horizontally...");
        Product outProduct = GPF.createProduct("Flip", parameters, product);
        System.out.println("Done.");
        
        /* Add Intensity virtual bands */
        String[] bandNames = outProduct.getBandNames();
        for (String bandName : bandNames){
            System.out.println(bandName);
            if(bandName.contains("i_")){
                String polarization = bandName.substring(2);
                // remove band, replace with virtual band
                outProduct.removeBand(outProduct.getBand("Intensity_"+polarization));                               
                outProduct.addBand("Intensity_"+polarization, "i_"+polarization+" * i_"+polarization+" + q_"+polarization+" * q_"+polarization+"", ProductData.TYPE_FLOAT32);               
                System.out.println("Added virtual band : Intensity_"+polarization);
            }
        }
        
        /* Update product description and name */
        String date = Utils.getTimeStamp();

        outProduct.setDescription(product.getDescription() + "Product flipped horizontally (" + date + ").\n");
        outProduct.setName(product.getName() + "_Hflip");
        return outProduct;
    }    
}
