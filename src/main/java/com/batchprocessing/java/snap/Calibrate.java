/*
 * Apply radiometric calibration to product
 * - complex: Calibrate product with complex output (Product)
 */
package com.batchprocessing.java.snap;

import java.io.File;
import java.util.HashMap;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.gpf.GPF;

/**
 *
 * @author dufobeso
 * S. Dufour-Beauséjour
 * PhD student at INRS-ETE
 * Québec, Canada
 * s.dufour.beausejour@gmail.com
 */
public class Calibrate {
    
    /* Calibrate and output in complex */
    public static Product complex(Product product,File imageFile){
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();
              
        /* Operator parameters */
        HashMap parameters = new HashMap();
        if (imageFile.toString().contains("RS2")){
            parameters.put("sourceBands","i_HH,q_HH,i_HV,q_HV,i_VH,q_VH,i_VV,q_VV");
        } else if (imageFile.toString().contains("TSX")){
            parameters.put("sourceBands",Utils.getSourceBands(product));            
        }
        
        parameters.put("outputImageInComplex","True");
        
        
        System.out.println("Calibrating...");
        Product outProduct = GPF.createProduct("Calibration", parameters, product);        
        System.out.println("Done.");
        
        /* Update product description and name */
        String date = Utils.getTimeStamp();
        
        outProduct.setDescription(product.getDescription()+"Product calibrated (" + date + ").\n");
        outProduct.setName(product.getName()+"_cal");
        return outProduct;        
    }   
}
