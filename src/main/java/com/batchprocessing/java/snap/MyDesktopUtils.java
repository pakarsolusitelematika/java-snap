/*
 * Convert virtual band into real band
 * Copied from SNAP desktop
 * - convertComputedBandIntoBand : convert a virtual band to a real band ()
 * - createSourceImage 
 * - createDescription
 * - getProductSceneViewTopComponent
 */
package com.batchprocessing.java.snap;

import com.bc.ceres.glevel.MultiLevelImage;
import org.esa.snap.core.datamodel.Band;
import org.esa.snap.core.datamodel.ImageInfo;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.datamodel.ProductNodeGroup;
import org.esa.snap.core.datamodel.RasterDataNode;
import org.esa.snap.core.datamodel.VirtualBand;
import org.esa.snap.netbeans.docwin.WindowUtilities;
import org.esa.snap.rcp.windows.ProductSceneViewTopComponent;

/**
 *
 * Copied from SNAP 
 * "Converts a virtual band into a "real" band.
 *
 * @author marcoz"
 */
public class MyDesktopUtils {

    public static void convertComputedBandIntoBand(Band computedBand) {
        String bandName = computedBand.getName();
        int width = computedBand.getRasterWidth();
        int height = computedBand.getRasterHeight();

        Band realBand = new Band(bandName, computedBand.getDataType(), width, height);
        realBand.setDescription(MyDesktopUtils.createDescription(computedBand));
        realBand.setValidPixelExpression(computedBand.getValidPixelExpression());
        realBand.setUnit(computedBand.getUnit());
        realBand.setSpectralWavelength(computedBand.getSpectralWavelength());
        realBand.setGeophysicalNoDataValue(computedBand.getGeophysicalNoDataValue());
        realBand.setNoDataValueUsed(computedBand.isNoDataValueUsed());
        if (computedBand.isStxSet()) {
            realBand.setStx(computedBand.getStx());
        }

        ImageInfo imageInfo = computedBand.getImageInfo();
        if (imageInfo != null) {
            realBand.setImageInfo(imageInfo.clone());
        }

        //--- Check if all the frame with the raster data are close
        Product product = computedBand.getProduct();
        ProductSceneViewTopComponent topComponent = getProductSceneViewTopComponent(computedBand);
        if (topComponent != null) {
            topComponent.close();
        }

        ProductNodeGroup<Band> bandGroup = product.getBandGroup();
        int bandIndex = bandGroup.indexOf(computedBand);
        bandGroup.remove(computedBand);
        bandGroup.add(bandIndex, realBand);

        realBand.setSourceImage(createSourceImage(computedBand, realBand));

        realBand.setModified(true);              
    }
    
    public static MultiLevelImage createSourceImage(Band computedBand, Band realBand) {
        if (computedBand instanceof VirtualBand) {
            return VirtualBand.createSourceImage(realBand, ((VirtualBand) computedBand).getExpression());
        }else {
            return computedBand.getSourceImage();
        }
    }    
    
    public static String createDescription(Band computedBand) {
        if (computedBand instanceof VirtualBand) {
            VirtualBand virtualBand = (VirtualBand) computedBand;
            String oldDescription = virtualBand.getDescription();
            String newDescription = oldDescription == null ? "" : oldDescription.trim();
            String formerExpressionDescription = "(expression was '" + virtualBand.getExpression() + "')";
            newDescription = newDescription.isEmpty() ? formerExpressionDescription : newDescription + " " + formerExpressionDescription;
            return newDescription;
        } else {
            return computedBand.getDescription();
        }
    }
    
    //copied from TimeSeriesManagerForm
    public static ProductSceneViewTopComponent getProductSceneViewTopComponent(RasterDataNode raster) {
        return WindowUtilities.getOpened(ProductSceneViewTopComponent.class)
                .filter(topComponent -> raster == topComponent.getView().getRaster())
                .findFirst()
                .orElse(null);
    }
    
    
    
}
