/*
 * Collocate products
 * - stack : return collocation of input product array (product)
 */
package com.batchprocessing.java.snap;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import org.apache.commons.lang3.ArrayUtils;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.datamodel.ProductData;
import org.esa.snap.core.gpf.GPF;

/**
 *
 * @author dufobeso
 * S. Dufour-Beauséjour
 * PhD student at INRS-ETE
 * Québec, Canada
 * s.dufour.beausejour@gmail.com
 */
public class Collocation {
    
    public static Product stack(String bayName, Product[] products, String bandToStack, String imageNames) throws IOException{
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();
      
        /* Remove unwanted bands */
        String[] bandsToRemove  = {"LabeledClasses","Confidence","mask_0","mask_1","mask_2","SigmaHH","SigmaHV","SigmaVH","SigmaVV","Span","PhiHHVV","PhiHHHV","HHVVcorrelation","Entropy","Anisotropy","Alpha","Alpha1","Alpha2","Alpha3","Lambda1","Lambda2","Lambda3","latitude","longitude"};;
        
        
        bandsToRemove = ArrayUtils.removeElement(bandsToRemove, bandToStack);
        for (String s : bandsToRemove ) {
            for (int k = 0; k<products.length; k++){
                if (products[k].containsBand(s)) {
                    products[k].removeBand(products[k].getBand(s));
                }                
            }
        }       
        System.out.println("Removed all bands except "+bandToStack);
        
        /* Operator parameters */
        HashMap parameters = new HashMap();
        HashMap productMap = new HashMap();
        parameters.put("extent","Master");
        parameters.put("initialOffsetMethod","Product Geolocation");        
        parameters.put("masterBands",products[0].getBand(bandToStack).getName());
        parameters.put("resamplingType","NEAREST_NEIGHBOUR");
        
        productMap.put("master",products[0]);
        Product[] slaveProducts = Arrays.copyOfRange(products, 1, products.length);
        productMap.put("slave",slaveProducts);
              
        
        System.out.println("Creating stack...");
        Product outProduct = GPF.createProduct("CreateStack", parameters, products);        
        System.out.println("Done.");
        
        /* Update product description and name */
        String date = Utils.getTimeStamp();
        
        outProduct.setDescription("Collocated : \n"+ imageNames +" (" + date + ").\n");
        outProduct.setName("Stack_"+bayName+"_"+bandToStack+imageNames);
        
        for(String s : outProduct.getBandNames()) {
            System.out.println(s);
            }
        
        return outProduct;   
        
    }
    
}
