/*
 * Adapted from PolarimetricParametersOp
 * Get polarimetric parameters from relative scattering matrix
 * - HH/HV : 
 * - sigma HH naught, HV
 * - HH/HV ratio
 * - HH HV phase difference
 * - VH/VV : 
 * - sigma VH naught, VV
 * - VV/VH ratio
 * - VV VH phase difference
 */
package com.batchprocessing.java.snap;

/**
 *
 * @author dufobeso
 * S. Dufour-Beauséjour
 * PhD student at INRS-ETE
 * Québec, Canada
 * s.dufour.beausejour@gmail.com
 */

import com.bc.ceres.core.ProgressMonitor;
import org.esa.s1tbx.io.PolBandUtils;
import org.esa.snap.core.datamodel.Band;
import org.esa.snap.core.datamodel.MetadataElement;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.datamodel.ProductData;
import org.esa.snap.core.gpf.Operator;
import org.esa.snap.core.gpf.OperatorException;
import org.esa.snap.core.gpf.OperatorSpi;
import org.esa.snap.core.gpf.Tile;
import org.esa.snap.core.gpf.annotations.OperatorMetadata;
import org.esa.snap.core.gpf.annotations.Parameter;
import org.esa.snap.core.gpf.annotations.SourceProduct;
import org.esa.snap.core.gpf.annotations.TargetProduct;
import org.esa.snap.core.util.ProductUtils;
import org.esa.snap.engine_utilities.datamodel.AbstractMetadata;
import org.esa.snap.engine_utilities.gpf.FilterWindow;
import org.esa.snap.engine_utilities.gpf.InputProductValidator;
import org.esa.snap.engine_utilities.gpf.OperatorUtils;
import org.esa.snap.engine_utilities.gpf.TileIndex;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.apache.commons.math3.complex.Complex;

/**
 * Compute polarimetric parameters for both quad-pol and compact-pol products.
 */

@OperatorMetadata(alias = "My-Polarimetric-Parameters-Dual",
        category = "Radar/Polarimetric",
        description = "Compute general polarimetric parameters for dual product")

public final class MyPolarimetricParametersDualOp extends Operator {

    private static final double pi = Math.PI;
    
    @SourceProduct(alias = "source")
    private Product sourceProduct;
    @TargetProduct
    private Product targetProduct;

    @Parameter(description = "Use mean relative scattering matrix", defaultValue = "true", label = "Use Mean Matrix")
    private boolean useMeanMatrix = true;

    @Parameter(valueSet = {"3", "5", "7", "9", "11", "13", "15", "17", "19"}, defaultValue = "5", label = "Window Size X")
    private String windowSizeXStr = "5";

    @Parameter(valueSet = {"3", "5", "7", "9", "11", "13", "15", "17", "19"}, defaultValue = "5", label = "Window Size Y")
    private String windowSizeYStr = "5";

    
    @Parameter(description = "Output backscattering coefficient sigmaHH", defaultValue = "false", label = "sigmaHH")
    private boolean outputSigmaHH = false;
    @Parameter(description = "Output backscattering coefficient sigmaHV", defaultValue = "false", label = "sigmaHV")
    private boolean outputSigmaHV = false;
    @Parameter(description = "Output backscattering coefficient sigmaVH", defaultValue = "false", label = "sigmaVH")
    private boolean outputSigmaVH = false;
    @Parameter(description = "Output backscattering coefficient sigmaVV", defaultValue = "false", label = "sigmaVV")
    private boolean outputSigmaVV = false;
    
    @Parameter(description = "Output Cross-Pol HH/HV", defaultValue = "false", label = "HH/HV Ratio")
    private boolean outputHHHVRatio = false;
    @Parameter(description = "Output Cross-Pol VV/VH", defaultValue = "false", label = "VV/VH Ratio")
    private boolean outputVVVHRatio = false;

    @Parameter(description = "Output Cross-Pol phase difference phiHHHV", defaultValue = "false", label = "phiHHHV")
    private boolean outputPhiHHHV = false;
   
    @Parameter(description = "Output Cross-Pol phase difference phiVVVH", defaultValue = "false", label = "phiVVVH")
    private boolean outputPhiVVVH = false;
    

    private FilterWindow window;
    private int sourceImageWidth = 0;
    private int sourceImageHeight = 0;

    private boolean isComplex;
    private PolBandUtils.MATRIX sourceProductType = null;
    private PolBandUtils.PolSourceBand[] srcBandList;
    //private Band hhBand = null, hvBand = null, vvBand = null, vhBand = null;

    private enum BandType {SigmaHH, SigmaHV, SigmaVH, SigmaVV, HHHVRatio, VVVHRatio, PhiHHHV, PhiVVVH}

    @Override
    public void initialize() throws OperatorException {

        try {
            final InputProductValidator validator = new InputProductValidator(sourceProduct);
            validator.checkIfSARProduct();
            isComplex = validator.isComplex();
            System.out.println("isComplex : "+isComplex);

            sourceProductType = PolBandUtils.getSourceProductType(sourceProduct);
            System.out.println("sourceProductType : " + sourceProductType);
            /*
            if (outputSpan || outputPedestalHeight || outputRVI) {
                if (sourceProductType == PolBandUtils.MATRIX.LCHCP || sourceProductType == PolBandUtils.MATRIX.RCHCP ||
                        sourceProductType == PolBandUtils.MATRIX.C2) {
                    throw new OperatorException("A quad-pol product is expected as input.");
                } else if (sourceProductType == PolBandUtils.MATRIX.C3 || sourceProductType == PolBandUtils.MATRIX.T3 ||
                        sourceProductType == PolBandUtils.MATRIX.FULL) {
                    if(!isComplex && (outputSpan || outputPedestalHeight)) {
                        throw new OperatorException("A T3,C3, or quad-pol slc product is expected as input for span and pedistal height.");
                    }
                } else {
                    throw new OperatorException("A quad-pol product is expected as input.");
                }
            }*/
            
            /*
            for (Band srcBand : sourceProduct.getBands()) {
                String unit = srcBand.getUnit();
                String bandName = srcBand.getName().toUpperCase();
                if (unit.equals(Unit.INTENSITY)) {
                    if (bandName.contains("_HH")) {
                        hhBand = srcBand;
                    } else if (bandName.contains("_HV")) {
                        hvBand = srcBand;
                    } else if (bandName.contains("_VV")) {
                        vvBand = srcBand;
                    } else if (bandName.contains("_VH")) {
                        vhBand = srcBand;
                    }
                }
            }*/

            /*
            if ((outputHHVVRatio || outputCSI || outputBMI) && (hhBand == null || vvBand == null)) {
                throw new OperatorException("Input product containing HH and VV bands is required");
            }
            if ((outputRFDI || outputHHHVRatio) && (hhBand == null || hvBand == null)) {
                throw new OperatorException("Input product containing HH and HV bands is required");
            }
            if (outputVVVHRatio && (vvBand == null || vhBand == null)) {
                throw new OperatorException("Input product containing VV and VH bands is required");
            }
            if (outputVSI && (hhBand == null || vvBand == null || hvBand == null || vhBand == null)) {
                throw new OperatorException("Input product containing HH, VV, HV and VH bands is required");
            }*/

            srcBandList = PolBandUtils.getSourceBands(sourceProduct, sourceProductType);
            System.out.println("srcBandList example: "+srcBandList[0].srcBands[1].getDisplayName());
            window = new FilterWindow(Integer.parseInt(windowSizeXStr), Integer.parseInt(windowSizeYStr));
            System.out.println("window : "+window.getWindowSizeX()+"x"+window.getWindowSizeY());
            sourceImageWidth = sourceProduct.getSceneRasterWidth();
            sourceImageHeight = sourceProduct.getSceneRasterHeight();
            System.out.println("sourceImageWidth : "+sourceImageWidth);
            System.out.println("sourceImageHeigth : "+sourceImageHeight);

            createTargetProduct();

            updateTargetProductMetadata();
        } catch (Throwable e) {
            OperatorUtils.catchOperatorException(getId(), e);
        }
    }

    /**
     * Create target product.
     */
    private void createTargetProduct() {

        targetProduct = new Product(sourceProduct.getName(),
                                    sourceProduct.getProductType(),
                                    sourceProduct.getSceneRasterWidth(),
                                    sourceProduct.getSceneRasterHeight());

        addSelectedBands();

        ProductUtils.copyProductNodes(sourceProduct, targetProduct);
    }

    /**
     * Add bands to the target product.
     *
     * @throws OperatorException The exception.
     */
    private void addSelectedBands() throws OperatorException {

        final String[] targetBandNames = getTargetBandNames();

        for (PolBandUtils.PolSourceBand bandList : srcBandList) {
            final Band[] targetBands = OperatorUtils.addBands(targetProduct, targetBandNames, bandList.suffix);
            bandList.addTargetBands(targetBands);
        }
        
        // HH/HV, or VH/VV
        if(outputSigmaHH && outputSigmaHV){
            targetProduct.addBand("HHHVRatio", "SigmaHH/SigmaHV", ProductData.TYPE_FLOAT32);
        } else if (outputSigmaVH && outputSigmaVV){
            targetProduct.addBand("VVVHRatio", "SigmaVV/SigmaVH", ProductData.TYPE_FLOAT32);
        } else {
            System.out.println("Unexpected values for outputSigmaXX.");
        }           

        if(targetProduct.getNumBands() == 0) {
            throw new OperatorException("No output bands selected");
        }
    }

    private String[] getTargetBandNames() {
        final List<String> targetBandNameList = new ArrayList<>(13);

        if (outputSigmaHH) {
            targetBandNameList.add(BandType.SigmaHH.toString());
        }
        if (outputSigmaHV) {
            targetBandNameList.add(BandType.SigmaHV.toString());
        }
        if (outputSigmaVH) {
            targetBandNameList.add(BandType.SigmaVH.toString());
        }
        if (outputSigmaVV) {
            targetBandNameList.add(BandType.SigmaVV.toString());
        }
        /*if (outputHHHVRatio) {
            targetBandNameList.add(BandType.HHHVRatio.toString());
        }
        if (outputVVVHRatio) {
            targetBandNameList.add(BandType.VVVHRatio.toString());
        }*/
        if (outputPhiHHHV) {
            targetBandNameList.add(BandType.PhiHHHV.toString());
        }
        if (outputPhiVVVH) {
            targetBandNameList.add(BandType.PhiVVVH.toString());
        }        

        return targetBandNameList.toArray(new String[targetBandNameList.size()]);
    }

    /**
     * Update metadata in the target product.
     */
    private void updateTargetProductMetadata() {

        final MetadataElement absRoot = AbstractMetadata.getAbstractedMetadata(targetProduct);
        if (absRoot != null) {
            absRoot.setAttributeInt(AbstractMetadata.polsarData, 1);
        }
        PolBandUtils.saveNewBandNames(targetProduct, srcBandList);
    }

    @Override
    public void computeTileStack(Map<Band, Tile> targetTiles, Rectangle targetRectangle, ProgressMonitor pm)
            throws OperatorException {

        final int x0 = targetRectangle.x;
        final int y0 = targetRectangle.y;
        final int w = targetRectangle.width;
        final int h = targetRectangle.height;
        final int maxY = y0 + h;
        final int maxX = x0 + w;
        //System.out.println("x0 = " + x0 + ", y0 = " + y0 + ", w = " + w + ", h = " + h);
        
        final TileIndex trgIndex = new TileIndex(targetTiles.get(targetProduct.getBandAt(0)));
        
        final double[][] Cr = new double[2][2];
        final double[][] Ci = new double[2][2];

        final Rectangle sourceRectangle = window.getSourceTileRectangle(x0, y0, w, h, sourceImageWidth, sourceImageHeight);

        final boolean computePolarimetricParam = true;

        for (final PolBandUtils.PolSourceBand bandList : srcBandList) {
            try {
                // save tile data for quicker access
                final TileData[] tileDataList = new TileData[bandList.targetBands.length];
                int i = 0;
                for (Band targetBand : bandList.targetBands) {
                    final Tile targetTile = targetTiles.get(targetBand);
                    tileDataList[i++] = new TileData(targetTile, targetBand.getName());
                }

                final Tile[] sourceTiles = new Tile[bandList.srcBands.length];
                final ProductData[] dataBuffers = new ProductData[bandList.srcBands.length];
                for (int j = 0; j < bandList.srcBands.length; j++) {
                    final Band srcBand = bandList.srcBands[j];
                    sourceTiles[j] = getSourceTile(srcBand, sourceRectangle);
                    dataBuffers[j] = sourceTiles[j].getDataBuffer();
                }
                final TileIndex srcIndex = new TileIndex(sourceTiles[0]);
                PolarimetricParameters param = null;

                for (int y = y0; y < maxY; ++y) {
                    trgIndex.calculateStride(y);
                    srcIndex.calculateStride(y);
                    for (int x = x0; x < maxX; ++x) {
                        final int tgtIdx = trgIndex.getIndex(x);

                        if (computePolarimetricParam) {
                            if (useMeanMatrix) {
                                /*PolOpUtils.getMeanCoherencyMatrix(x, y,
                                                                  window.getHalfWindowSizeX(), window.getHalfWindowSizeY(),
                                                                  sourceImageWidth, sourceImageHeight, sourceProductType,
                                                                  srcIndex, dataBuffers, Tr, Ti);*/
                                MyPolOpUtils.getMeanCovarianceMatrixC2(x, y, window.getHalfWindowSizeX(), window.getHalfWindowSizeY(), 
                                        sourceImageWidth, sourceImageHeight, sourceProductType, srcIndex, dataBuffers, Cr, Ci);
                                /*MyPolOpUtils.getMeanScatteringMatrix(x, y, window.getHalfWindowSizeX(), window.getHalfWindowSizeY(),
                                                                  sourceImageWidth, sourceImageHeight, sourceProductType,
                                                                  srcIndex, dataBuffers, Sr, Si);*/
                                
                            } else {
                                throw new OperatorException("Only the processing done with the mean matrix has been programmed.");
                            }

                            param = computePolarimetricParameters(Cr, Ci);
                            /*System.out.println("Cr : ");
                            for (int q = 0; q < Cr.length; q++){
                                for (int j = 0; j < Cr.length; j++){
                                    System.out.print(Cr[q][j] + " ");
                                }    
                                System.out.println();
                            }
                            System.out.println("Ci : ");
                            for (int q = 0; q < Ci.length; q++){
                                for (int j = 0; j < Ci.length; j++){
                                    System.out.print(Ci[q][j] + " ");
                                }    
                                System.out.println();
                            }*/
                        }

                        for (final TileData tileData : tileDataList) {

                            if (outputSigmaHH && tileData.bandType.equals(BandType.SigmaHH)){
                                tileData.dataBuffer.setElemFloatAt(tgtIdx, (float) param.sigmaHH);
                            }
                            if (outputSigmaHV && tileData.bandType.equals(BandType.SigmaHV)){
                                tileData.dataBuffer.setElemFloatAt(tgtIdx, (float) param.sigmaHV);
                            }
                            if (outputSigmaVH && tileData.bandType.equals(BandType.SigmaVH)){
                                tileData.dataBuffer.setElemFloatAt(tgtIdx, (float) param.sigmaVH);
                            }
                            if (outputSigmaVV && tileData.bandType.equals(BandType.SigmaVV)){
                                tileData.dataBuffer.setElemFloatAt(tgtIdx, (float) param.sigmaVV);
                            }
                            if (outputPhiHHHV && tileData.bandType.equals(BandType.PhiHHHV)) {
                                tileData.dataBuffer.setElemFloatAt(tgtIdx, (float) param.PhiHHHV);
                            }
                            if (outputPhiVVVH && tileData.bandType.equals(BandType.PhiVVVH)) {
                                tileData.dataBuffer.setElemFloatAt(tgtIdx, (float) param.PhiVVVH);
                            }                                                    
                        }
                    }
                }

            } catch (Throwable e) {
                OperatorUtils.catchOperatorException(getId(), e);
            }
        }
    }

    private static class TileData {
        final Tile tile;
        final ProductData dataBuffer;
        final String bandName;
        final BandType bandType;

        public TileData(final Tile tile, final String bandName) {
            this.tile = tile;
            this.dataBuffer = tile.getDataBuffer();
            this.bandName = bandName;
            this.bandType = BandType.valueOf(bandName);
        }
    }

    /**
     * Compute general polarimetric parameters for given covariance matrix.
     *
     * @param Cr Real part of the mean covariance matrix.
     * @param Ci Imaginary part of the mean covariance matrix.
     * @return The general polarimetric parameters.
     */
    public PolarimetricParameters computePolarimetricParameters(final double[][] Cr, final double[][] Ci) {

        final PolarimetricParameters parameters = new PolarimetricParameters();
               
        // HH/HV, or VH/VV
        if(outputSigmaHH && outputSigmaHV){
            parameters.sigmaHH = 4*pi*Cr[0][0];
            parameters.sigmaHV = 4*pi*Cr[1][1];
            parameters.PhiHHHV = Math.atan(Ci[0][1]/Cr[0][1]);
        } else if (outputSigmaVH && outputSigmaVV){
            parameters.sigmaVH = 4*pi*Cr[0][0];
            parameters.sigmaVV = 4*pi*Cr[1][1];
            parameters.PhiVVVH = Math.atan(Ci[1][0]/Cr[1][0]);
        } else {
            System.out.println("Unexpected values for outputSigmaXX.");
        }      

        /*
        System.out.println("sigmaHH : "+parameters.sigmaHH);
        System.out.println("sigmaHV : "+parameters.sigmaHV);
        System.out.println("sigmaVV : "+parameters.sigmaVV);
        System.out.println("Span : "+parameters.Span);
        System.out.println("PhiHHVV : "+parameters.PhiHHVV);
        System.out.println("PhiHHHV : "+parameters.PhiHHHV);
        System.out.println("HHVVcorrelation : "+parameters.HHVVcorrelation);
        */
        
        return parameters;
    }

    public static class PolarimetricParameters {
        public double sigmaHH;
        public double sigmaHV;
        public double sigmaVH;
        public double sigmaVV;
        //public double HHHVratio;
        //public double VVVHratio;
        public double PhiHHHV;
        public double PhiVVVH;
    }

    /**
     * The SPI is used to register this operator in the graph processing framework
     * via the SPI configuration file
     * {@code META-INF/services/org.esa.snap.core.gpf.OperatorSpi}.
     * This class may also serve as a factory for new operator instances.
     *
     * @see OperatorSpi#createOperator()
     * @see OperatorSpi#createOperator(Map, Map)
     */
    public static class Spi extends OperatorSpi {
        public Spi() {
            super(MyPolarimetricParametersDualOp.class);
        }
    }
}