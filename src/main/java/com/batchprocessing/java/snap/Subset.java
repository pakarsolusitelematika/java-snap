/*
 * Create an image subset
 * - create: Creates the image subset (Product)
 * - getGeometry: Gets the geometry to be used to subset the product, depending on the bay (String)
 * - keepHH : Keeps only the HH bands for products for InSAR processing (Product[])
 */
package com.batchprocessing.java.snap;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import java.io.File;
import java.util.HashMap;
import org.esa.snap.core.datamodel.Band;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.datamodel.ProductNode;
import org.esa.snap.core.gpf.GPF;

/**
 *
 * @author dufobeso
 * S. Dufour-Beauséjour
 * PhD student at INRS-ETE
 * Québec, Canada
 * s.dufour.beausejour@gmail.com
 */
public class Subset {
    
    /* Create the subset */
    public static Product create(Product product, File imageFile) throws ParseException{
        
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();
        
        /* GetGeometry from product */
        String subsetWKT = Subset.getGeometry(imageFile);        
        Geometry subsetGeometry = new WKTReader().read(subsetWKT);
                
        /* Operator parameters */
        HashMap parameters = new HashMap();
        parameters.put("copyMetadata","True");
        parameters.put("geoRegion",subsetGeometry);
                
        System.out.println("Creating subset...");
        Product outProduct = GPF.createProduct("Subset", parameters, product);        
        System.out.println("Done.");
        
        /* Update product description and name */
        String date = Utils.getTimeStamp();
        
        outProduct.setDescription(product.getDescription()+"Product subset created (" + date + ").\n");
        outProduct.setName(product.getName()+"_sub");
        return outProduct;          
        
    }
    
    /* Get geometry to be used to create the subset */
    public static String getGeometry(File imageFile){
                     
        String imageName = Utils.getImageName(imageFile);
        String bayName = Utils.getBay(imageFile);
        System.out.println("bayName is : "+bayName);
        
        String subsetGeometry = null;
        /* Try matching imageName to expected dates and thus identify the bay. */
        if (bayName == "Salluit"){           
            // Smaller subset
            /*subsetGeometry = ("POLYGON ((-75.87663446984794 62.33189890425399, "
                    + "-75.48378024467557 62.29421922184036, -75.5622399060796 62.12103547443131, "
                    + "-75.9528549808958 62.158609712559944, -75.87663446984794 62.33189890425399))");*/            
            // Larger subset which includes ice from the strait
            subsetGeometry = ("POLYGON ((-75.81628069026742 62.35353074134657, -75.4440942595849 62.317737698687644, "
                    + "-75.52960304511728 62.12955617718936, -75.89948723552679 62.16523710049654, "
                    + "-75.81628069026742 62.35353074134657))");           
        } else if (bayName == "Kangiqsujuaq"){
            // Smaller subset
            /*subsetGeometry = ("POLYGON ((-72.24226845296766 61.74254088535121, "
                    + "-71.84446247291646 61.7048985071332, -71.92337000318552 61.52147649254185, "
                    + "-72.31883163120708 61.55901740807304, -72.24226845296766 61.74254088535121))");*/
            // Larger subset which includes ice from the strait            
            subsetGeometry = ("POLYGON ((-72.20172852400779 61.76765185266755, -71.80881454934936 61.73040755720904,"
                    + "-71.89181985368324 61.538031062006915, -72.28229751813967 61.57516467617474, "
                    + "-72.20172852400779 61.76765185266755))");
        } else if (bayName == "DeceptionBay"){
            subsetGeometry = ("POLYGON ((-74.92832719375963 62.27354545544764, "
                    + "-74.40968626069508 62.22378697070371, -74.47360412491176 62.082911154221584, "
                    + "-74.98984940040964 62.13255339088457, -74.92832719375963 62.27354545544764))");
        } else {
            System.out.println("Could not match imageName to expected date and thus could not identify the bay.");
            // This should cause the processing to stop, but for now it doesn't.
        }
        
                
        /* Return geometry string */
        return subsetGeometry;
    }   
    
    /* Create subset of products - keep HH only*/
    public static Product[] keepHH(Product[] imageProducts){
                
        String bandsToKeep = "i_HH,q_HH";
        
        for (Product product : imageProducts) {
            String[] bandNames = product.getBandNames();
            
            for (String bandName : bandNames ){
                if (!(bandsToKeep.contains(bandName))){
                    Band bandToRemove = product.getBand(bandName);
                    product.removeBand(bandToRemove);
                }
            } 
        }
        
        return imageProducts;
    }
    
}
