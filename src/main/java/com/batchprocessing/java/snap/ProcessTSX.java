/**
 *
 * @author dufobeso
 * S. Dufour-Beauséjour
 * PhD student at INRS-ETE
 * Québec, Canada
 * s.dufour.beausejour@gmail.com
 *
 * TSX quad-pol image processing with SNAP through Java
 * Processing steps already coded :
 * - Read TSX product
 * - Write to BEAM-DIMAP
 * - Apply radiometric calibration
 * - Convert to polarimetric matrix (C2)
 * - Apply speckle filter
 * - Compute polarimetric parameters from C3 matrix (ratios, phase differences, correlation, etc)
 * 
 * To do : 
 * - Fix Flip methods; explicitely give source band names (i,q) then add virtual band for intensity ?
 * - Quicklooks as in ProcessRS2; what bands should be used ?. 
 * - Orthorectification
 * - Get intersection of product with water mask
 * - Classification 
 *
 */
package com.batchprocessing.java.snap;

import com.vividsolutions.jts.io.ParseException;
import java.io.File;
import java.io.IOException;
import org.esa.snap.core.datamodel.Product;

public class ProcessTSX {

    public static void main(String[] args) throws IOException, ParseException {
        File imageFile = new File(args[0]);

        String imageFound = new String(imageFile.getName());
        System.out.println("Found matching image : " + imageFound);
        String imageName = Utils.getImageName(imageFile);
        System.out.println("Shorter image name is : " + imageName);

        /* Create working directory if it doesn't exist */
        File workingDirectory = Utils.createWorkingDirectory(imageFile);

        /* Create a readme.txt file if it doesn't exist */
        Utils.createReadme(workingDirectory);

        /* Read the TSX product if it hasn't been written to BEAM-DIMAP yet */
        String processingDone = new String();
        File dimFile = new File(workingDirectory, imageName + processingDone + ".dim");
        if (!(dimFile.exists())) {
            Product product = ReadProduct.TSX(imageFile);
            Utils.writeDualType(product, imageFile);

            processingDone += "";
            WriteProduct.BEAMDIMAP(product, workingDirectory, dimFile);
        }
        
        /*Flip the product horizontally */
        File flipFile = new File(workingDirectory, imageName + processingDone + "_Hflip.dim");
        if (!(flipFile.exists())){
            Product product = ReadProduct.TSX(imageFile);
            Product flipProduct = Flip.horizontally(product, imageFile);
            
            WriteProduct.BEAMDIMAP(flipProduct, workingDirectory, flipFile);            
        }
        processingDone += "_Hflip";        
        
        
        /*Write dual RGB quicklook : HH, HV, VV*/
        /*File quickFile = new File(workingDirectory, imageName + processingDone+".jpg");
        if (!(quickFile.exists())) {
            Product product = ReadProduct.BEAMDIMAP(imageFile, processingDone);
            String polarizationNames = Utils.getPolarizationNames(product);
            System.out.println("Writing dual quicklook before subset : " + polarizationNames + "...");          
            WriteProduct.quicklookDualRGB(product, quickFile, polarizationNames);
        }*/   

        /* Calibrate product and write to BEAM-DIMAP */
        File calFile = new File(workingDirectory, imageName + processingDone + "_cal.dim");
        if (!(calFile.exists())) {
            Product product = ReadProduct.BEAMDIMAP(imageFile, processingDone);
            Product calProduct = Calibrate.complex(product, imageFile);

            WriteProduct.BEAMDIMAP(calProduct, workingDirectory, calFile);
        }
        processingDone += "_cal";
        
        /* Convert to polarimetric matrix and write to BEAM-DIMAP */
        File matFile = new File(workingDirectory, imageName + processingDone + "_C2.dim");
        if (!(matFile.exists())) {
            Product subProduct = ReadProduct.BEAMDIMAP(imageFile, processingDone);
            Product matProduct = Matrix.C2(subProduct, imageFile);

            WriteProduct.BEAMDIMAP(matProduct, workingDirectory, matFile);
        }
        processingDone += "_C2";
        

        /* Apply polarimetric speckle filter :  Polarimetric Refined Lee 7x7 and write to BEAM-DIMAP */
        File spkFile = new File(workingDirectory, imageName + processingDone + "_spk.dim");
        if (!(spkFile.exists())) {
            Product matProduct = ReadProduct.BEAMDIMAP(imageFile, processingDone);
            Product spkProduct = SpeckleFilter.PolarimetricRefinedLee(matProduct, imageFile);

            WriteProduct.BEAMDIMAP(spkProduct, workingDirectory, spkFile);
        }
        processingDone += "_spk";
        
        /* Polarimetric parameters */
        File paramFile = new File(workingDirectory, imageName + processingDone + "_param.dim");
        if (!(paramFile.exists())) {
            Product spkProduct = ReadProduct.BEAMDIMAP(imageFile, processingDone);

            // Get general parameters and HAalpha decomposition parameters from same source product                    
            Product genParamProduct = Polarimetric.parametersDual(spkProduct, imageFile);
            Product HalphaProduct = Polarimetric.HalphaC2(spkProduct, imageFile);

            Product[] sourceProducts = new Product[1];
            sourceProducts[0] = HalphaProduct;
            Product paramProduct = Polarimetric.combine(genParamProduct, sourceProducts, imageFile, processingDone);

            WriteProduct.BEAMDIMAP(paramProduct, workingDirectory, paramFile);
        }
        processingDone += "_param";               
    }
    
    public static void DLR(String[] args) throws IOException, ParseException {
        File imageFile = new File(args[0]);
        String seriesName = args[1];

        String imageFound = new String(imageFile.getName());
        System.out.println("Found matching image : " + imageFound);
        String imageName = Utils.getImageName(imageFile);
        System.out.println("Shorter image name is : " + imageName);

        /* Create working directory if it doesn't exist */
        File workingDirectory = Utils.createWorkingDirectory(imageFile);

        /* Create a readme.txt file if it doesn't exist */
        Utils.createReadme(workingDirectory);

        /* Read the TSX product if it hasn't been written to BEAM-DIMAP yet */
        String processingDone = "_processed";
        File dimFile = new File(workingDirectory, imageName + processingDone + ".dim");
        if (!(dimFile.exists())) {
            Product product = ReadProduct.TIFF(imageFile);
            
            processingDone += "";
            //WriteProduct.BEAMDIMAP(product, workingDirectory, dimFile);
        }
    
        /*Write dual RGB quicklook for Kennaugh elements*/
        String[] bandNames = Utils.getBandNames(seriesName);
        String RGBbandNames = Utils.getKennaughRGB(seriesName);
        File quickFile = new File(workingDirectory, imageName + processingDone + "_"+ RGBbandNames+".jpg");
        
        if (!(quickFile.exists())) {
            Product product = ReadProduct.TIFF(imageFile,bandNames);
            System.out.println("Writing dual quicklook : " + bandNames + "...");          
            WriteProduct.quicklookDLR(product, quickFile, bandNames);
        }
    }
}
