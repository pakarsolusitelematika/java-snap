/*
 * Writing products
 * - BEAMDIMAP : write to BEAM-DIMAP ()
 * - geoTIFF : write to GeoTIFF ()
 * - geoTIFFStack : write to GeoTIFF ()
 * - quicklookKMeans : write quicklooks of the KMeans classification ()
 * - quicklookMLCclasses : write quicklooks of the MLC classification classes ()
 * - quicklookMLCconfidence : write quicklook of the MLC classif confidence()
 * - quicklookDLR : write quicklook of the first three bands in DLR processed TSX data ()
 * - quicklookDualRGB : write quicklook of the two polarization bands with a third empty one ()
 * - quicklookRGB : write quicklooks of three bands in product ()
 * - quicklookRGBwOverlay : write quicklooks of three bands with vector overlays ()
 */
package com.batchprocessing.java.snap;

import com.bc.ceres.core.ProgressMonitor;
import com.bc.ceres.glayer.CollectionLayer;
import com.bc.ceres.glayer.LayerContext;
import com.bc.ceres.glayer.support.ImageLayer;
import com.bc.ceres.glevel.MultiLevelModel;
import com.bc.ceres.grender.support.BufferedImageRendering;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import org.esa.snap.core.dataio.ProductIO;
import org.esa.snap.core.datamodel.Band;
import org.esa.snap.core.datamodel.ImageInfo;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.datamodel.ColorPaletteDef.Point;
import org.esa.snap.core.util.ProductUtils;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.util.HashMap;
import javax.media.jai.JAI;
import org.esa.snap.core.datamodel.ColorPaletteDef;
import org.esa.snap.core.datamodel.SceneTransformProvider;
import org.esa.snap.core.datamodel.VectorDataNode;
import org.esa.snap.core.gpf.GPF;
import org.esa.snap.core.gpf.common.BandMathsOp.BandDescriptor;
import org.esa.snap.core.image.ImageManager;
import org.esa.snap.ui.product.VectorDataLayer;


/**
 *
 * @author dufobeso
 * S. Dufour-Beauséjour
 * PhD student at INRS-ETE
 * Québec, Canada
 * s.dufour.beausejour@gmail.com
 */
public class WriteProduct {
        
    public static void BEAMDIMAP(Product product, File workingDirectory, File outFile) throws IOException{        
        /* Get shorter imageName and create outFile from workingDirectory, imageName, .dim*/ 
        //File workingDirectory = Utils.createWorkingDirectory(imageFile);
                
        /* Writing image product*/        
        if (!(outFile.exists())) {

            System.out.println("Writing to BEAM-DIMAP ...");
            ProductIO.writeProduct(product, outFile, "BEAM-DIMAP", true);
            System.out.println("Done.");

            /* Update readme.txt */
            File readmeFile = Utils.updateReadme(workingDirectory);
            String date = Utils.getTimeStamp();

            try (BufferedWriter readme = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(readmeFile, true), StandardCharsets.UTF_8))) {
                readme.append("Wrote product " + outFile.getName() + "  (" + date + ")\n");
            }
        }        
    }
    
    public static void geoTIFF(Product product, File imageFile, File outFile) throws IOException {                
        /* Writing image product*/        
        if (!(outFile.exists())) {
            
            System.out.println("Writing to GeoTIFF ...");
            ProductIO.writeProduct(product, outFile, "GeoTIFF", true);
            System.out.println("Done.");

            /* imageFile is null if working with stacked products; no read me */
            File workingDirectory = Utils.createWorkingDirectory(imageFile);

            /* Update readme.txt */
            File readmeFile = Utils.updateReadme(workingDirectory);
            String date = Utils.getTimeStamp();

            try (BufferedWriter readme = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(readmeFile, true), StandardCharsets.UTF_8))) {
                readme.append("Wrote product " + outFile.getName() + "  (" + date + ")\n");
            }

        }
    }
    
    public static void geoTIFFStack(Product product, File outFile) throws IOException {                
        /* Writing image product*/        
        if (!(outFile.exists())) {
            
            System.out.println("Writing to GeoTIFF...");
            ProductIO.writeProduct(product, outFile, "GeoTIFF", true);
            System.out.println("Done.");
        }     
    }
        
    public static void quicklookKMeans(Product product, File classFile) throws IOException{       
        Color gray = new Color(77,77,77);
        Color blue = new Color(93,165,218);
        Color orange = new Color(250,164,58);
        Color green = new Color(96,189,104);
        Color pink = new Color(241,124,176);
        Color brown = new Color(178,145,47);
        Color purple = new Color(178,118,178);
        Color yellow = new Color(222,207,63);
        Color red = new Color(241,88,84);
        Color lightGray = new Color(200,200,200);
                
        Point[] points = new Point[10];
        points[0] = new Point(0.0, red , "1");
        points[1] = new Point(1.0, blue, "2");
        points[2] = new Point(2.0, yellow, "3");
        points[3] = new Point(3.0, green, "4");
        points[4] = new Point(4.0, purple, "5");
        points[5] = new Point(5.0, gray, "6");
        points[6] = new Point(6.0, orange,"7");
        points[7] = new Point(7.0, pink, "8");
        points[8] = new Point(8.0, brown, "9");
        points[9] = new Point(9.0, lightGray, "10");
        ImageInfo information = new ImageInfo(new ColorPaletteDef(points, 10));
        
        Band[] bands = new Band[1];
        bands[0] = product.getBand("class_indices");
        RenderedImage rendered = ImageManager.getInstance().createColoredBandImage(bands, information, 0);
        
        // Note : to get correct colors, PNG must be used, but passing a File with an .jpg extension writes a jpg file from the PNG content
        JAI.create("filestore", rendered, classFile.toString(), "PNG");
        
    }    
    
    
    public static void quicklookMLCclasses(Product product, File classFile) throws IOException{
        Point[] points = new Point[3];
        Color purple = new Color(98,46,179);
        Color lightPink = new Color(186,166,187);
        Color darkBlue = new Color(8,56,151);
        
        points[0] = new Point(0.0, purple , "InnerIce");
        points[1] = new Point(1.0, lightPink, "MiddleIce");
        points[2] = new Point(2.0, darkBlue, "OuterIce");
        ImageInfo information = new ImageInfo(new ColorPaletteDef(points, 3));
        
        Band[] bands = new Band[1];
        bands[0] = product.getBand("LabeledClasses");
        RenderedImage rendered = ImageManager.getInstance().createColoredBandImage(bands, information, 0);
        
        // Note : to get correct colors, PNG must be used, but passing a File with an .jpg extension writes a jpg file from the PNG content
        JAI.create("filestore", rendered, classFile.toString(), "PNG");     
    }
    
    public static void quicklookMLCconfidence(Product product, File quickFile) throws IOException {
        Band[] bands = new Band[3];
        bands[0] = product.getBand("Confidence");
        bands[1] = bands[0];
        bands[2] = bands[0];
        ImageInfo information = ProductUtils.createImageInfo(bands, true, ProgressMonitor.NULL);       
        RenderedImage rendered = ImageManager.getInstance().createColoredBandImage(bands, information, 0);
        
        JAI.create("filestore", rendered, quickFile.toString(), "PNG");
    }
    
    public static void quicklookDLR(Product product, File rgbFile, String[] polarizationNames) throws IOException{
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();
        Band[] bands = new Band[3];
        if (polarizationNames[0].contains("K0")){
            // RGB : K1 (Absorption), K0 (VV+VH), K5 (Phase Delay)
            bands[0] = product.getBand("K1");
            bands[1] = product.getBand("K0");
            bands[2] = product.getBand("K5");   
        }
       
        ImageInfo information = ProductUtils.createImageInfo(bands, true, ProgressMonitor.NULL);       
        RenderedImage rendered = ImageManager.getInstance().createColoredBandImage(bands, information, 0);
        
        JAI.create("filestore", rendered, rgbFile.toString(), "PNG");
    }    
    
    public static void quicklookDualRGB(Product product, File rgbFile, String polarizationNames) throws IOException{
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();
        Band[] bands = new Band[3];

        if (polarizationNames.contains("HH,HV")) {
            bands[0] = product.getBand("Intensity_HH");
            bands[1] = product.getBand("Intensity_HV");
            
            BandDescriptor targetBand = new BandDescriptor();
            targetBand.name = "Zero";
            targetBand.expression = "Intensity_HV*0";
            targetBand.type = "float32";
            BandDescriptor[] targetBands ={targetBand};
                       
            HashMap parameters = new HashMap();
            parameters.put("targetBands",targetBands);
            Product emptyBandProduct = GPF.createProduct("BandMaths", parameters, product);          
           
            bands[2] = emptyBandProduct.getBand("Zero");
        } else if (polarizationNames.contains("VH,VV")) {
            // Create a product with a band full of zeros to use as third color in RBB for dual polarization
            BandDescriptor targetBand = new BandDescriptor();
            targetBand.name = "Zero";
            targetBand.expression = "Intensity_VH*0";
            targetBand.type = "float32";
            BandDescriptor[] targetBands ={targetBand};
                       
            HashMap parameters = new HashMap();
            parameters.put("targetBands",targetBands);
            Product emptyBandProduct = GPF.createProduct("BandMaths", parameters, product);    
           
            bands[0] = emptyBandProduct.getBand("Zero");
            bands[1] = product.getBand("Intensity_VH");
            bands[2] = product.getBand("Intensity_VV");
        } else {
            System.out.println("Unexpected polarization names in WriteProduct.quicklookDualRGB");
        }

        ImageInfo information = ProductUtils.createImageInfo(bands, true, ProgressMonitor.NULL);       
        RenderedImage rendered = ImageManager.getInstance().createColoredBandImage(bands, information, 0);
        
        JAI.create("filestore", rendered, rgbFile.toString(), "PNG");
    }
    
    public static void quicklookRGB(Product product, File rgbFile, String redBandName, String greenBandName, String blueBandName) throws IOException{

        Band[] bands = new Band[3];

        bands[0] = product.getBand(redBandName);
        bands[1] = product.getBand(greenBandName);
        bands[2] = product.getBand(blueBandName);    

        ImageInfo information = ProductUtils.createImageInfo(bands, true, ProgressMonitor.NULL);       
        RenderedImage rendered = ImageManager.getInstance().createColoredBandImage(bands, information, 0);
        
        JAI.create("filestore", rendered, rgbFile.toString(), "PNG");   
    }
    
    public static void quicklookRGBwOverlay(Product product, File rgbFile, String[] RGBbandNames, String[] vectorNames, String classif) throws IOException{
        System.out.println("Node names are : ");
        for ( String s: product.getVectorDataGroup().getNodeDisplayNames()){
            System.out.println(s);
        };
        /* Create RGB image */
        Band[] bands = new Band[3];

        for (int k = 0; k < RGBbandNames.length; k++){
            bands[k] = product.getBand(RGBbandNames[k]);
        }   

        ImageInfo information = ProductUtils.createImageInfo(bands, true, ProgressMonitor.NULL);       
        RenderedImage renderedRGB = ImageManager.getInstance().createColoredBandImage(bands, information, 0);
        
        
        /* Create image layers*/
        CollectionLayer collectionLayer = new CollectionLayer();       
        LayerContext ctx = new ImageUtils.MyLayerContext(product, collectionLayer); 
        SceneTransformProvider provider = bands[0];
        MultiLevelModel multiLevelModel = bands[0].getMultiLevelModel();
        
        // Vector
        String[] colors = ImageUtils.getColorsHEX(classif);
        int k =0;        
        for (String vectorName : vectorNames) {
            VectorDataNode vectorDataNode = product.getVectorDataGroup().get(vectorName);
            //String styleCSS = String.format("fill:#ffffff; fill-opacity:0.0; stroke:%s; stroke-opacity:1.0; stroke-width:20.0; symbol:cross", colors[k]);                        
            String styleCSS = String.format("fill:%s; fill-opacity:0.5; stroke:#ffffff; stroke-opacity:1.0; stroke-width:10.0; symbol:cross", colors[k]);
            vectorDataNode.setDefaultStyleCss(styleCSS);
            VectorDataLayer vectorDataLayer = new VectorDataLayer(ctx, vectorDataNode, provider );
            collectionLayer.getChildren().add(vectorDataLayer);
            k++;
            //System.out.println("Style CSS : "+vectorDataLayer.getVectorDataNode().getStyleCss());
        }

        // RGB
        ImageLayer RGBLayer = new ImageLayer(renderedRGB,bands[0].getImageToModelTransform(),1);
        collectionLayer.getChildren().add(RGBLayer);       
        
        /* Create the complete image by overlaying */        
        BufferedImage buffered = new BufferedImage(renderedRGB.getWidth(),renderedRGB.getHeight(),BufferedImage.TYPE_INT_ARGB);
        BufferedImageRendering rendering = ImageUtils.createRendering(buffered,multiLevelModel);
        collectionLayer.render(rendering);
        
        JAI.create("filestore",rendering.getImage(),rgbFile.toString(),"PNG");
    }   
   
}
    
