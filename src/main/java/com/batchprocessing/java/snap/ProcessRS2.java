/**
 *
 * @author dufobeso
 * S. Dufour-Beauséjour
 * PhD student at INRS-ETE
 * Québec, Canada
 * s.dufour.beausejour@gmail.com
 *
 * RS2 quad-pol image processing with SNAP through Java
 * Processing steps already coded :
 * - Read RS2 product
 * - Write to BEAM-DIMAP
 * - Create a subset
 * - Apply radiometric calibration
 * - Convert to polarimetric matrix (C3)
 * - Apply polarimetric speckle filter
 * - Apply H A alpha decomposition to C3 matrix
 * - Compute polarimetric parameters from C3 matrix (ratios, phase differences, correlation, etc)
 * - Orthorectification
 * - Get intersection of product with water mask
 * - Classification 
 *
 */
package com.batchprocessing.java.snap;

import com.vividsolutions.jts.io.ParseException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import org.esa.snap.core.dataio.ProductIO;
import org.esa.snap.core.datamodel.Band;
import org.esa.snap.core.datamodel.Product;

public class ProcessRS2 {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";

    @SuppressWarnings("empty-statement")
    public static void main(String[] args) throws IOException, ParseException {
        File imageFile = new File(args[0]);

        String imageFound = new String(imageFile.getName());
        System.out.println("Found matching image : " + imageFound);
        String imageName = Utils.getImageName(imageFile);
        System.out.println("Shorter image name is : " + imageName);

        /* Create working directory if it doesn't exist */
        File workingDirectory = Utils.createWorkingDirectory(imageFile);

        /* Create a readme.txt file if it doesn't exist */
        Utils.createReadme(workingDirectory);

        /* Read the RS2 product if it hasn't been written to BEAM-DIMAP yet */
        String processingDone = new String();
        File dimFile = new File(workingDirectory, imageName + processingDone + ".dim");
        if (!(dimFile.exists())) {
            Product product = ReadProduct.RS2(imageFile);

            processingDone += "";
            WriteProduct.BEAMDIMAP(product, workingDirectory, dimFile);
        }

        /*Write RGB quicklook : HH, HV, VV*/
        File quickFile = new File(workingDirectory, imageName + processingDone+".jpg");
        if (!(quickFile.exists())) {
            Product product = ReadProduct.BEAMDIMAP(imageFile, processingDone);
            System.out.println("Writing quicklook before subset : HH HV VV...");
            WriteProduct.quicklookRGB(product, quickFile, "Intensity_HH", "Intensity_HV", "Intensity_VV");
        }       
        
        
        /* Create subset of product and write to BEAM-DIMAP */
        File subFile = new File(workingDirectory, imageName + processingDone + "_sub.dim");
        if (!(subFile.exists())) {
            Product calProduct = ReadProduct.BEAMDIMAP(imageFile, processingDone);
            Product subProduct = Subset.create(calProduct, imageFile);

            WriteProduct.BEAMDIMAP(subProduct, workingDirectory, subFile);
        }
        processingDone += "_sub";

        /* Calibrate product and write to BEAM-DIMAP */
        File calFile = new File(workingDirectory, imageName + processingDone + "_cal.dim");
        if (!(calFile.exists())) {
            Product product = ReadProduct.BEAMDIMAP(imageFile, processingDone);
            Product calProduct = Calibrate.complex(product, imageFile);

            WriteProduct.BEAMDIMAP(calProduct, workingDirectory, calFile);
        }
        processingDone += "_cal";

        /* Convert to polarimetric matrix and write to BEAM-DIMAP */
        File matFile = new File(workingDirectory, imageName + processingDone + "_C3.dim");
        if (!(matFile.exists())) {
            Product subProduct = ReadProduct.BEAMDIMAP(imageFile, processingDone);
            Product matProduct = Matrix.C3(subProduct, imageFile);

            WriteProduct.BEAMDIMAP(matProduct, workingDirectory, matFile);
        }
        processingDone += "_C3";

        /* Apply polarimetric speckle filter : Refined Lee 7x7 and write to BEAM-DIMAP */
        File spkFile = new File(workingDirectory, imageName + processingDone + "_spk.dim");
        if (!(spkFile.exists())) {
            Product matProduct = ReadProduct.BEAMDIMAP(imageFile, processingDone);
            Product spkProduct = SpeckleFilter.PolarimetricRefinedLee(matProduct, imageFile);

            WriteProduct.BEAMDIMAP(spkProduct, workingDirectory, spkFile);
        }
        processingDone += "_spk";

        /* Polarimetric parameters */
        File paramFile = new File(workingDirectory, imageName + processingDone + "_param.dim");
        if (!(paramFile.exists())) {
            Product spkProduct = ReadProduct.BEAMDIMAP(imageFile, processingDone);

            // Get general parameters and HAalpha decomposition parameters from same source product                    
            Product genParamProduct = Polarimetric.parameters(spkProduct, imageFile);
            Product HAalphaProduct = Polarimetric.HAalpha(spkProduct, imageFile);

            Product[] sourceProducts = new Product[1];
            sourceProducts[0] = HAalphaProduct;
            Product paramProduct = Polarimetric.combine(genParamProduct, sourceProducts, imageFile, processingDone);

            WriteProduct.BEAMDIMAP(paramProduct, workingDirectory, paramFile);
        }
        processingDone += "_param";

        /*Write RGB quicklook : HH, HV, VV*/
        quickFile = new File(workingDirectory, imageName +"_sub_cal_C3_spk__HH_HV_VV.jpg");
        if (!(quickFile.exists())) {
            Product paramProduct = ReadProduct.BEAMDIMAP(imageFile, processingDone);
            System.out.println("Writing quicklook : HH HV VV...");
            WriteProduct.quicklookRGB(paramProduct, quickFile, "SigmaHH", "SigmaHV", "SigmaVV");
        }

        /*Write RGB quicklook : HH, HV, Entropy*/
        quickFile = new File(workingDirectory, imageName + processingDone + ".jpg");
        if (!(quickFile.exists())) {
            Product paramProduct = ReadProduct.BEAMDIMAP(imageFile, processingDone);
            System.out.println("Writing quicklook : HH HV Entropy...");
            WriteProduct.quicklookRGB(paramProduct, quickFile, "SigmaHH", "SigmaHV", "Entropy");
        }

        /* Orthorectify product */
        File TCFile = new File(workingDirectory, imageName + processingDone + "_TC.dim");
        if (!(TCFile.exists())) {
            Product paramProduct = ReadProduct.BEAMDIMAP(imageFile, processingDone);
            Product orthoProduct = Orthorectify.RDTC(paramProduct, imageFile, processingDone);

            WriteProduct.BEAMDIMAP(orthoProduct, workingDirectory, TCFile);
        }

        processingDone += "_TC";

        /*Write to GeoTIFF only HH, HV, VV*/
        File tiffFile = new File(workingDirectory, imageName + processingDone + ".tif");
        if (!(tiffFile.exists())) {
            Product mskProduct = ReadProduct.BEAMDIMAP(imageFile, processingDone);
            for (Band candidateBand : mskProduct.getBands()) {
                if (!candidateBand.getName().contains("Sigma")) {
                    mskProduct.removeBand(candidateBand);
                }
            }
            WriteProduct.geoTIFF(mskProduct, imageFile, tiffFile);
        }

        /*Write RGB quicklook : HH, HV, VV after TC*/
        quickFile = new File(workingDirectory, imageName + processingDone + ".jpg");
        if (!(quickFile.exists())) {
            Product TCProduct = ReadProduct.BEAMDIMAP(imageFile, processingDone);
            System.out.println("Writing quicklook : HH HV VV after TC ...");
            WriteProduct.quicklookRGB(TCProduct, quickFile, "SigmaHH", "SigmaHV", "SigmaVV");
        }

        /* Multiply product by water mask */
        // Writing of this product is done in Masking.getIntersection
        File mskFile = new File(workingDirectory, imageName + processingDone + "_msk.dim");

        if (!(mskFile.exists())) {
            Product TCProduct = ReadProduct.BEAMDIMAP(imageFile, processingDone);
            TCProduct = Masking.addWater(TCProduct, imageFile);
            TCProduct = Masking.addROI(TCProduct, imageFile);
            Product mskProduct = Masking.getIntersection2(TCProduct, imageFile, processingDone);
        }
        processingDone += "_msk";

        /*Write RGB quicklook : HH, HV, VV after msk*/
        quickFile = new File(workingDirectory, imageName + processingDone + ".jpg");
        if (!(quickFile.exists())) {
            Product mskProduct = ReadProduct.BEAMDIMAP(imageFile, processingDone);
            System.out.println("Writing quicklook : HH HV VV after msk ...");
            WriteProduct.quicklookRGB(mskProduct, quickFile, "SigmaHH", "SigmaHV", "SigmaVV");
        }

        /*Write to GeoTIFF */
        tiffFile = new File(workingDirectory, imageName + processingDone + ".tif");
        if (!(tiffFile.exists())) {
            Product mskProduct = ReadProduct.BEAMDIMAP(imageFile, processingDone);
            WriteProduct.geoTIFF(mskProduct, imageFile, tiffFile);
        }

        /* Classify product with maximum likelihood or K-Means */
        // Choose either _mlc, _mlc2 or _km. For km, choose numClass
        // Specify classifier bands; warning, only Kmeans classifier takes this into account for now
        String classif = "_km";
        String[] classifierBands = {"SigmaHH", "SigmaHV", "SigmaVV"};
        int numClass = 8;

        String classifDirectory = null;
        File classifFile = null;
        File kmTiffFile = null;
        File kmRGBFile = null;
        String[] types = null;
        String logName = imageName;
        if (classif.contains("mlc")) {
            classifFile = new File(workingDirectory, imageName + processingDone + classif + ".dim");

            String[] types3 = {"InnerIce", "MiddleIce", "OuterIce"};
            String[] types2 = {"InnerMiddleIce", "OuterIce"};

            if ("_mlc".equals(classif)) {
                types = types3;
                logName += "_mlc";
            } else if ("_mlc2".equals(classif)) {
                types = types2;
                logName += "_mlc2";
            }
        } else if (classif.contains("km")) {
            classif += numClass;
            classifDirectory = "";
            for (String s : classifierBands) {
                classifDirectory += s;
                classifDirectory += "_";
            }
            classifDirectory = classifDirectory.substring(0, classifDirectory.length() - 1);
            Utils.createDirectory(imageFile, classifDirectory);
            classifFile = new File(workingDirectory, classifDirectory + "\\\\" + imageName + processingDone + classif + ".dim");
            kmTiffFile = new File(workingDirectory, classifDirectory + "\\\\" + imageName + processingDone + classif + ".tif");
            kmRGBFile = new File(workingDirectory, classifDirectory + "\\\\" + imageName + processingDone + classif + ".jpg");
        }

        /*Write RGB quicklook : HH, HV, Entropy + training areas*/
        //quickFile = new File(workingDirectory, imageName + processingDone.substring(0, processingDone.length()-3)+ "_wTrainingAreas.png");
        quickFile = new File(workingDirectory, imageName + processingDone + "_wTrainingAreas.jpg");
        //quickFile = new File(workingDirectory, imageName + processingDone+ ".jpg");
        if (!(quickFile.exists())) {
            if (classif.contains("mlc")) {
                Product orthoProduct = ReadProduct.BEAMDIMAP(imageFile, processingDone);
                System.out.println("Writing quicklook : HH HV Entropy + training areas...");
                HashMap map = Utils.addTrainingVectors(orthoProduct, imageFile, types);
                String[] RGBbands = {"SigmaHH", "SigmaHV", "Entropy"};
                //String vectorName =((String[])map.get("vectorNames"))[0];
                //String vectorName = "Water_HudsonBay_1";

                WriteProduct.quicklookRGBwOverlay((Product) map.get("product"), quickFile, RGBbands, (String[]) map.get("vectorNames"), classif);
            }
        }

        if (!(classifFile.exists())) {
            Product mskProduct = ReadProduct.BEAMDIMAP(imageFile, processingDone);
            Product classifProduct = null;
            Product outProduct = null;
            if (classif.contains("mlc")) {
                outProduct = Classify.maxLikelihood(mskProduct, imageFile, types, logName);
                numClass = types.length;
            } else if (classif.contains("km")) {
                outProduct = Classify.KMeans(mskProduct, imageFile, numClass, classifierBands);
            }

            // add band masks for each class    
            classifProduct = Classify.addClassMasks(outProduct, numClass, classif);
            WriteProduct.BEAMDIMAP(classifProduct, workingDirectory, classifFile);
        }

        processingDone += classif;

        /*Write RGB quicklook*/
        quickFile = new File(workingDirectory, imageName + processingDone + ".jpg");
        if (classif.contains("km")) {
            quickFile = kmRGBFile;
        }

        if (!(quickFile.exists())) {
            System.out.println("Writing quicklook : classif...");
            Product classifProduct = ProductIO.readProduct(classifFile);

            if (classif.contains("mlc")) {
                WriteProduct.quicklookMLCclasses(classifProduct, quickFile);
            } else if (classif.contains("km")) {
                WriteProduct.quicklookKMeans(classifProduct, quickFile);
            }
        }

        /* Write mlc confidence quicklook */
        quickFile = new File(workingDirectory, imageName + processingDone + "confidence.jpg");
        if (!(quickFile.exists())) {
            if (classif.contains("mlc")) {
                Product classifProduct = ReadProduct.BEAMDIMAP(imageFile, processingDone);
                System.out.println("Writing quicklook : mlc confidence...");
                WriteProduct.quicklookMLCconfidence(classifProduct, quickFile);
            }
        }

        /*Write to GeoTIFF */
        if (classif.contains("km")) {
            tiffFile = kmTiffFile;
        } else {
            tiffFile = new File(workingDirectory, imageName + processingDone + ".tif");
        }

        if (!(tiffFile.exists())) {
            Product classifProduct = ProductIO.readProduct(classifFile);
            WriteProduct.geoTIFF(classifProduct, imageFile, tiffFile);
        }

        /* Run the time series processing method described below */
        //timeSeriesProcessing();
    }

    public static void timeSeriesProcessing() throws IOException {
        System.out.println("TimeSeriesProcessing...");

        //String[] bandsToStack = {"LabeledClasses","Confidence"};
        String[] bandsToStack = {"SigmaHH", "SigmaHV", "SigmaVV", "Entropy", "Anisotropy", "Alpha"};
        String[] bayNames = {"Salluit", "Kangiqsujuaq", "DeceptionBay"};

        String processingDone = "_sub_cal_C3_spk_param_TC_msk_mlc";
        File directory = new File("Z:\\\\Donnees\\\\Imagerie\\\\RADARSAT-2\\\\");

        for (String bay : bayNames) {
            String pattern = Utils.getBayPattern(bay);
            System.out.println("pattern : " + pattern);

            /* Look for images matching regex in image directory : */
            File[] imageFiles = Utils.findMatchingImages(directory, pattern);
            for (File file : imageFiles) {
                System.out.println(file);
            }

            String bayName = Utils.getBay(imageFiles[0]);
            System.out.println("Bay name : " + bayName);

            for (String bandToStack : bandsToStack) {
                Product[] products = new Product[imageFiles.length];
                int k = 0;
                String imageName = null;
                String imageNames = "";

                for (File imageFile : imageFiles) {
                    imageName = Utils.getImageName(imageFile);
                    imageNames += "_" + imageName.substring(3);
                    products[k] = ReadProduct.BEAMDIMAP(imageFile, processingDone);
                    //System.out.println(products[k].getBandNames()[0]);
                    k++;
                }

                String stackName = null;
                if (bandToStack.contains("LabeledClasses")) {
                    stackName = "Stack_" + bayName + "_" + "mlc" + imageNames;
                } else if (bandToStack.contains("Confidence")) {
                    stackName = "Stack_" + bayName + "_" + "mlcconfidence" + imageNames;
                } else {
                    stackName = "Stack_" + bayName + "_" + bandToStack + imageNames;
                }

                File outFile = new File(directory, "Analyse\\\\ImageFiles\\\\" + stackName + ".dim");
                File workingDirectory = new File(directory, "Analyse\\\\ImageFiles\\\\");
                
                /* Create a readme.txt file if it doesn't exist */
                Utils.createReadme(workingDirectory);          
                

                if (!(outFile.exists())) {
                    Product stackedProduct = Collocation.stack(bayName, products, bandToStack, imageNames);
                    WriteProduct.BEAMDIMAP(stackedProduct, workingDirectory, outFile);
                }

                /*Write to GeoTIFF */
                outFile = new File(directory, "Analyse\\\\ImageFiles\\\\" + stackName + ".tif");
                System.out.println("outFile name is " + outFile.getName());
                if (!(outFile.exists())) {
                    Product outProduct = ReadProduct.BEAMDIMAPStack(stackName);
                    WriteProduct.geoTIFFStack(outProduct, outFile);
                }
            }
        }
    }
}
