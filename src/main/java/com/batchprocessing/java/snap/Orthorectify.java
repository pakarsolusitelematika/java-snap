/*
 * Orthorectify product through geometric terrain correction
 * - RDTC: Create product from Range-Doppler Terrain-Correction (Product)
 */
package com.batchprocessing.java.snap;

import java.io.File;
import java.util.HashMap;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.gpf.GPF;
import org.esa.snap.core.util.SystemUtils;
import org.openide.util.Lookup;

/**
 *
 * @author dufobeso
 * S. Dufour-Beauséjour
 * PhD student at INRS-ETE
 * Québec, Canada
 * s.dufour.beausejour@gmail.com
 */
public class Orthorectify {  
    
    public static Product RDTC(Product product, File imageFile, String processingDone){
        SystemUtils.initGeoTools();
        MySystemUtils.initJAI(Lookup.getDefault().lookup(ClassLoader.class));
        //SystemUtils.initJAI(ClassLoader.getSystemClassLoader());
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();
                
        File DEMFile = new File("Z:\\\\Donnees\\\\Imagerie\\\\DEM\\\\mnt2.tif");
        //File DEMFile = new File("C:\\\\Users\\\\dufobeso\\\\Desktop\\\\RS2_Analysis\\\\DEM\\\\mnt2.tif");
        
        /* Create bandName list in string for parameter input */
        String[] sourceBandNames = product.getBandNames();
        String sourceBandNamesParameter = new String();
        int k = 1;
        for (String sourceBandName : sourceBandNames){
            sourceBandNamesParameter += sourceBandName;
            
            if (k<sourceBandNames.length){
                sourceBandNamesParameter += ",";
            }
            k++;
        }
        System.out.println(sourceBandNamesParameter);
        
        
        /* Operator parameters*/
        HashMap parameters = new HashMap();
        parameters.put("demName","External DEM");
        parameters.put("externalDEMFile",DEMFile);
        parameters.put("nodataValueAtSea","false");
        parameters.put("outputComplex","false");
        parameters.put("pixelSpacingInMeter","8");
        parameters.put("saveLatLon","true");
        
        //parameters.put("sourceBands",sourceBandNamesParameter);
        parameters.put("sourceBands","SigmaHH,SigmaHV,SigmaVH,SigmaVV,Span,PhiHHVV,PhiHHHV,HHVVcorrelation,Entropy,Anisotropy,Alpha,Alpha1,Alpha2,Alpha3,Lambda1,Lambda2,Lambda3");
        //parameters.put("sourceBands","SigmaHH");
        System.out.println("Orthorectifying product...");
        Product outProduct = GPF.createProduct("Terrain-Correction", parameters, product);        
        System.out.println("Done.");
        
        /* Update product description and name */
        String date = Utils.getTimeStamp();
        
        outProduct.setDescription(product.getDescription()+"Product orthorectified (" + date + ").\n");
        outProduct.setName(product.getName()+"_TC");
        
        return outProduct;
    }
}
