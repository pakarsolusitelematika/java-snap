
/*
 * Extracting polarimetric parameters from the C3 matrix.
 * - parameters : create product containing the polarimetric parameters extracted from the C3 matrix : 
        sigmaHH,HV,VH,VV, HHVV, HHHV, VVVH ratios, Span, phiHHVV, phiHHHV, co-pol correlation (Product)
 * - parametersDual : create product containing the polarimetric parameters extracted from the C2 matrix : 
        sigmaHH,HV OR VH,VV, HHHV OR VVVH ratios, phiHHHV OR phiVVVH
 * - HAalpha : create product from the H A alpha decomposition of the C3 matrix (Product)
 * - HalphaC2 : create product from the H alpha decomposition of the C2 matrix (Product)
 * - getSrel : create and write relative scattering matrix from C3 (Product)
 * - combine : combine two products (parameters and HAalpha) into one (Product)
 */

package com.batchprocessing.java.snap;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import org.apache.commons.math3.complex.Complex;
import org.esa.snap.core.dataio.ProductIO;
import org.esa.snap.core.dataio.ProductWriter;
import org.esa.snap.core.datamodel.Band;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.datamodel.ProductData;
import org.esa.snap.core.gpf.GPF;
import org.esa.snap.core.util.ProductUtils;
/**
 *
 * @author dufobeso
 * S. Dufour-Beauséjour
 * PhD student at INRS-ETE
 * Québec, Canada
 * s.dufour.beausejour@gmail.com
 */
public class Polarimetric {
    
    private static final double sqrt2 = Math.sqrt(2);
    private static final double pi = Math.PI;
    
    /* Get polarimetric parameters (sigmaHH,HV,VH,VV, HHVV, HHHV, VVVH ratios, Span, phiHHVV, phiHHHV, co-pol correlation from C3 matrix */
    public static Product parameters(Product product, File imageFile) {
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();
        
        /* Operator parameters*/
        /* The default settings are all the ones I want */
        HashMap parameters = new HashMap();
        parameters.put("useMeanMatrix","true");
        parameters.put("windowSizeXStr","5");
        parameters.put("windowSizeXStr","5");
   
        
        System.out.println("Computing polarimetric parameters from C3 matrix...");
        Product outProduct = GPF.createProduct("My-Polarimetric-Parameters", parameters, product);        
        System.out.println("Done.");
        
        /* Update readme */
        String date = Utils.getTimeStamp();
        String productName = Utils.getImageName(imageFile);
        
        outProduct.setDescription(product.getDescription()+"Polarimetric parameters from C3 (" + date + ").\n");
        outProduct.setName(product.getName()+"_polparam");
        return outProduct;  
    }
    
    /* Get dual-pol polarimetric parameters (sigmaHH,HV OR VH,VV, HHHV OR VVVH ratios, phiHHHV OR phiVVVH) from C2 matrix */
    public static Product parametersDual(Product product, File imageFile) throws IOException {
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();
        String dualType = Utils.getDualType(imageFile);
        
        /* Operator parameters*/
        HashMap parameters = new HashMap();
        parameters.put("useMeanMatrix","true");
        parameters.put("windowSizeXStr","5");
        parameters.put("windowSizeXStr","5");
   
        if (dualType.contains("VH/VV")){
            parameters.put("outputSigmaVH",true);
            parameters.put("outputSigmaVV",true);
            parameters.put("outputVVVHRatio",true);
            parameters.put("outputPhiVVVH",true);            
        } else if (dualType.contains("HH/HV")){
            parameters.put("outputSigmaHH",true);
            parameters.put("outputSigmaHV",true);
            parameters.put("outputHHHVRatio",true);
            parameters.put("outputPhiHHHV",true);  
        } else {
            System.out.println("Unexpected dualType.");
        }
        
        System.out.println("Computing polarimetric parameters from C2 matrix for "+dualType+"...");
        Product outProduct = GPF.createProduct("My-Polarimetric-Parameters-Dual", parameters, product);        
        System.out.println("Done.");
        
        /* Update readme */
        String date = Utils.getTimeStamp();
        String productName = Utils.getImageName(imageFile);
        
        outProduct.setDescription(product.getDescription()+"Polarimetric parameters from C2 (" + date + ").\n");
        outProduct.setName(product.getName()+"_polparam");
        return outProduct;  
    }    
    
    
    /* Apply H A alpha decomposition to C3 matrix */
    public static Product HAalpha(Product product, File imageFile) {
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();
        
        /* Operator parameters*/
        HashMap parameters = new HashMap();
        parameters.put("decomposition","H-A-Alpha Quad Pol Decomposition");
        parameters.put("outputAlpha123","true");
        parameters.put("outputHAAlpha","true");
        parameters.put("outputLambda123","true");
        parameters.put("windowSize","5");
        
        
        System.out.println("Applying H A alpha polarimetric decomposition to C3 matrix...");
        Product outProduct = GPF.createProduct("Polarimetric-Decomposition", parameters, product);        
        System.out.println("Done.");
        
        /* Update readme */
        String date = Utils.getTimeStamp();
        String productName = Utils.getImageName(imageFile);
        
        outProduct.setDescription(product.getDescription()+"H A alpha decomposition of C3 (" + date + ").\n");
        outProduct.setName(product.getName()+"_decomp");
        return outProduct;     
        
    }
    
/* Apply H alpha decomposition to C2 matrix */
    public static Product HalphaC2(Product product, File imageFile) {
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();
        
        /* Operator parameters*/
        HashMap parameters = new HashMap();
        parameters.put("decomposition","H-Alpha Dual Pol Decomposition");
        parameters.put("windowSize","5");
               
        System.out.println("Applying H-Alpha Dual Pol Decomposition...");
        Product outProduct = GPF.createProduct("Polarimetric-Decomposition", parameters, product);        
        System.out.println("Done.");
        
        /* Update readme */
        String date = Utils.getTimeStamp();
        String productName = Utils.getImageName(imageFile);
        
        outProduct.setDescription(product.getDescription()+"H alpha decomposition of C2 (" + date + ").\n");
        outProduct.setName(product.getName()+"_decomp");
        return outProduct;            
    }    
    

    /* Convert the C3 matrix to Srel by creating a new product, element by element */
    public static Product getSrel(Product product, File imageFile, String processingDone) throws IOException{

        ////////// New product
        // Initialize new product
        System.out.println("Creating new product for Srel...");
        System.out.println("Product name : " + product.getName()+"_Srel");
        System.out.println("Product type : " + product.getProductType());
        System.out.println("Scene raster width : " + product.getSceneRasterWidth());
        System.out.println("Scene raster height : " + product.getSceneRasterHeight());
        
        Product outProduct = new Product(product.getName()+"_Srel", product.getProductType(), product.getSceneRasterWidth(), product.getSceneRasterHeight());
        //ProductUtils.copyGeoCoding(product, outProduct);
        //ProductUtils.copyMetadata(product, outProduct);
        ProductUtils.copyProductNodes(product, outProduct);
        
        // Set product writer and write header 
        ProductWriter writer = ProductIO.getProductWriter("BEAM-DIMAP");
        outProduct.setProductWriter(writer);
        
        // Utils
        File workingDirectory = Utils.createWorkingDirectory(imageFile);
        String imageName = Utils.getImageName(imageFile);
        File outFile = new File(workingDirectory, imageName+processingDone+"_Srel.dim");
        
        // Initialize all bands so that they are written to the header, then write the header
        Band[] targetBands = new Band[12];
        targetBands[0] = outProduct.addBand("i_HH", ProductData.TYPE_FLOAT32);
        targetBands[1] = outProduct.addBand("q_HH", ProductData.TYPE_FLOAT32);
        targetBands[2] = outProduct.addBand("Intensity_HH", "i_HH*i_HH + q_HH*q_HH", ProductData.TYPE_FLOAT32);
        targetBands[3] = outProduct.addBand("i_HV", ProductData.TYPE_FLOAT32);
        targetBands[4] = outProduct.addBand("q_HV", ProductData.TYPE_FLOAT32);
        targetBands[5] = outProduct.addBand("Intensity_HV", "i_HV*i_HV + q_HV*q_HV",ProductData.TYPE_FLOAT32);
        targetBands[6] = outProduct.addBand("i_VH", ProductData.TYPE_FLOAT32);
        targetBands[7] = outProduct.addBand("q_VH", ProductData.TYPE_FLOAT32);
        targetBands[8] = outProduct.addBand("Intensity_VH", "i_VH*i_VH + q_VH*q_VH",ProductData.TYPE_FLOAT32);
        targetBands[9] = outProduct.addBand("i_VV", ProductData.TYPE_FLOAT32);
        targetBands[10] = outProduct.addBand("q_VV", ProductData.TYPE_FLOAT32);
        targetBands[11] = outProduct.addBand("Intensity_VV", "i_VV*i_VV + q_VV*q_VV",ProductData.TYPE_FLOAT32);
           
        int[] realBands = {0,3,6,9};
        int[] imaginaryBands = {1,4,7,10};
        int[] intensityBands = {2,5,8,11};
        for (int n : intensityBands) {
            targetBands[n].setDescription("Intensity from complex data");
            targetBands[n].setUnit("intensity");
        }
        for (int n : realBands){
            targetBands[n].setUnit("real");
        }
        for (int n : imaginaryBands){
            targetBands[n].setUnit("imaginary");
        }
                
        outProduct.writeHeader(outFile);
        

        /* Writing the bands
        Scan each line of the mask to data buffer, then scan each element and normalize to 0 or 1, 
        multiply the mask element with the source band element and then write each line to the target band. 
        java arrays = HxW and getPixels operator is HxW */
        
        // Initialize data buffer arrays
        float[][] sourceDataBuffer = new float[9][product.getSceneRasterWidth()];        
        float[][] targetDataBuffer = new float[8][product.getSceneRasterWidth()];

        // Initialize complex variables
        Complex SHH;
        Complex SHV;
        Complex SVH;
        Complex SVV;
        
        // For each line
        for (int i = 0; i < product.getSceneRasterHeight(); i++) {
            // Read the line to the source data buffer
            sourceDataBuffer[0] = product.getBand("C11").readPixels(0, i, product.getSceneRasterWidth(), 1, sourceDataBuffer[0]);
            sourceDataBuffer[1] = product.getBand("C12_real").readPixels(0, i, product.getSceneRasterWidth(), 1, sourceDataBuffer[1]);
            sourceDataBuffer[2] = product.getBand("C12_imag").readPixels(0, i, product.getSceneRasterWidth(), 1, sourceDataBuffer[2]);
            sourceDataBuffer[3] = product.getBand("C13_real").readPixels(0, i, product.getSceneRasterWidth(), 1, sourceDataBuffer[3]);
            sourceDataBuffer[4] = product.getBand("C13_imag").readPixels(0, i, product.getSceneRasterWidth(), 1, sourceDataBuffer[4]);
            sourceDataBuffer[5] = product.getBand("C22").readPixels(0, i, product.getSceneRasterWidth(), 1, sourceDataBuffer[5]);
            sourceDataBuffer[6] = product.getBand("C23_real").readPixels(0, i, product.getSceneRasterWidth(), 1, sourceDataBuffer[6]);
            sourceDataBuffer[7] = product.getBand("C23_imag").readPixels(0, i, product.getSceneRasterWidth(), 1, sourceDataBuffer[7]);
            sourceDataBuffer[8] = product.getBand("C33").readPixels(0, i, product.getSceneRasterWidth(), 1, sourceDataBuffer[8]);
            
            // For each element in line
            for (int j = 0; j < product.getSceneRasterWidth(); j++) {
                
                // Put the source data in complex variables
                Complex C11 = new Complex(sourceDataBuffer[0][j],0);
                Complex C12 = new Complex(sourceDataBuffer[1][j],sourceDataBuffer[2][j]);
                Complex C13 = new Complex(sourceDataBuffer[3][j],sourceDataBuffer[4][j]);
                Complex C21;
                C21 = C12.conjugate();
                Complex C22 = new Complex(sourceDataBuffer[5][j],0);
                Complex C23 = new Complex(sourceDataBuffer[6][j],sourceDataBuffer[7][j]);
                Complex C33 = new Complex(sourceDataBuffer[8][j],0);
                
                // Compute Sii elements from Cii
                SHH = C11.sqrt();
                SHV = C21.divide(sqrt2).divide(C11.sqrt());
                SVH = C21.divide(sqrt2).divide(C11.sqrt());
                SVV = C13.divide(C11.sqrt());
                
                // Assign Sii real and imaginary parts to target data buffer
                targetDataBuffer[0][j] = (float)SHH.getReal();
                targetDataBuffer[1][j] = (float)SHH.getImaginary();
                targetDataBuffer[2][j] = (float)SHV.getReal();
                targetDataBuffer[3][j] = (float)SHV.getImaginary();
                targetDataBuffer[4][j] = (float)SVH.getReal();
                targetDataBuffer[5][j] = (float)SVH.getImaginary();
                targetDataBuffer[6][j] = (float)SVV.getReal();
                targetDataBuffer[7][j] = (float)SVV.getImaginary();
            }
            
            // Write the line for each band
            int k = 0;
            for (int w = 0; w < targetBands.length; w+=3) {
                
                //System.out.println("k : "+k);
                //System.out.println("w : "+w);
                targetBands[w].writePixels(0, i, product.getSceneRasterWidth(), 1, targetDataBuffer[k]);
                targetBands[w + 1].writePixels(0, i, product.getSceneRasterWidth(), 1, targetDataBuffer[k + 1]);
                k += 2;
            }

            //System.out.println(Arrays.toString(targetDataBuffer[0]));
            //System.out.println(Arrays.toString(targetDataBuffer[2]));  
        }
        product.closeIO();
        System.out.println("Done.");
        
        /* Update readme.txt */
        File readmeFile = Utils.updateReadme(workingDirectory);
        String date = Utils.getTimeStamp();

        try (BufferedWriter readme = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(readmeFile, true), StandardCharsets.UTF_8))) {
            readme.append("Wrote product " + outFile.getName() + "  (" + date + ")\n");
        }    
        
        System.out.println("Product bands are :");
        for (String s : outProduct.getBandNames()) {
            System.out.println(s);
        }

        return outProduct;
    }
    
    public static Product combine(Product masterProduct, Product[] sourceProducts, File imageFile, String processingDone) {
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();
            
        /* Operator parameters */
        HashMap parameters = new HashMap();
        
        /* Operator sourceProducts */
        HashMap sources = new HashMap();
        sources.put("masterProduct",masterProduct);
        sources.put("sourceProducts",sourceProducts[0]);

        
        System.out.print("Merging ");
        for (Product p : sourceProducts){
            System.out.print(p.getName()+" ");
        }
        System.out.print(" into "+masterProduct.getName()+"...");    
        Product outProduct = GPF.createProduct("Merge", parameters, sources);        
        System.out.println("Done.");
        
        /* Update product description and name */
        String date = Utils.getTimeStamp();
        
        outProduct.setDescription(masterProduct.getDescription()+"Computed polarimetric parameters (general and HAalpha or HalphaC2) (" + date + ").\n");
        outProduct.setName(masterProduct.getName()+"_param");
        return outProduct; 
    }
}