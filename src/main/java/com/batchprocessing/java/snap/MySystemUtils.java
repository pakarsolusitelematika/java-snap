/*
 * Modified tileSize and tileCacheSize 
 * Called in Orthorectify
 */
package com.batchprocessing.java.snap;

import java.awt.Dimension;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.text.MessageFormat;
import java.util.logging.Logger;
import javax.media.jai.JAI;
import static org.esa.snap.core.util.SystemUtils.getApplicationContextId;
import org.esa.snap.runtime.Config;

/**
 * A collection of (BEAM-) system level functions.
 * <p>
 * <p> All functions have been implemented with extreme caution in order to provide a maximum performance.
 *
 * @author Norman Fomferra
 * @author Sabine Embacher
 * 
 * Modified by :
 * @author Sophie Dufour-Beauséjour
 */
public class MySystemUtils {
    
    public static final String SNAP_PARALLELISM_PROPERTY_NAME = getApplicationContextId() + ".parallelism";
    public static final String SNAP_CACHE_DIR_PROPERTY_NAME = getApplicationContextId() + ".cachedir";    
    
    /**
    * The SNAP system logger. Default name is "org.esa.snap" which may be overridden by system property "snap.logger.name".
    */
    public static final Logger LOG = Config.instance().logger();
    
    private static class NullOutputStream extends OutputStream {
        @Override
        public void write(int b) throws IOException {
        }
    }
    
    
    public static void initJAI(ClassLoader cl) {
        // Suppress ugly (and harmless) JAI error messages saying that a JAI is going to continue in pure Java mode.
        System.setProperty("com.sun.media.jai.disableMediaLib", "true");  // disable native libraries for JAI

        final PrintStream originalErrStream = System.err;
        try (PrintStream nullErrStream = new PrintStream(new MySystemUtils.NullOutputStream())) {
            // suppress messages printed to the error stream during operator registration
            System.setErr(nullErrStream);
            JAI.getDefaultInstance().getOperationRegistry().registerServices(cl);
        } catch (Throwable t) {
            LOG.fine("Failed to register additional JAI operators: " + t.getMessage());
        } finally {
            System.setErr(originalErrStream);
        }
        int parallelism = Config.instance().preferences().getInt(SNAP_PARALLELISM_PROPERTY_NAME,
                                                                 Runtime.getRuntime().availableProcessors());
        JAI.getDefaultInstance().getTileScheduler().setParallelism(parallelism);
        LOG.fine(MessageFormat.format("JAI tile scheduler parallelism set to {0}", parallelism));

        long OneMiB = 1024L * 1024L;

        JAI.enableDefaultTileCache();
        Long size = Config.instance().preferences().getLong("snap.jai.tileCacheSize", 4096) * OneMiB;
        JAI.getDefaultInstance().getTileCache().setMemoryCapacity(size);

        final long tileCacheSize = JAI.getDefaultInstance().getTileCache().getMemoryCapacity() / OneMiB;
        LOG.fine(MessageFormat.format("JAI tile cache size is {0} MiB", tileCacheSize));

        final int tileSize = Config.instance().preferences().getInt("snap.jai.defaultTileSize", 512);
        JAI.setDefaultTileSize(new Dimension(tileSize, tileSize));
        LOG.fine(MessageFormat.format("JAI default tile size is {0} pixels", tileSize));

        JAI.getDefaultInstance().setRenderingHint(JAI.KEY_CACHED_TILE_RECYCLING_ENABLED, Boolean.TRUE);
        LOG.fine("JAI tile recycling enabled");
    }
    
}
