/*
 * Compute mean matrices
 * - getMeanCovarianceMatrix adapted getMeanCoherenceMatrix from PolOpUtils
 * - getMeanCovarianceMatrixDual "
 * - getCovarianceMatrix copied from PolOpUtils
 * - getCovarianceMatrixC2 adapted from PolOpUtils
 */
package com.batchprocessing.java.snap;

import Jama.Matrix;
import org.apache.commons.math3.util.FastMath;
import org.esa.s1tbx.io.PolBandUtils;
import org.esa.snap.core.datamodel.ProductData;
import org.esa.snap.engine_utilities.gpf.TileIndex;

/**
 *
 * @author dufobeso
 */
public class MyPolOpUtils {

    /**
     * Get mean covariance matrix for given pixel.
     *
     * @param x                 X coordinate of the given pixel.
     * @param y                 Y coordinate of the given pixel.
     * @param halfWindowSizeX   The sliding window size / 2.
     * @param halfWindowSizeY   The sliding window size / 2.
     * @param sourceProductType The source product type.
     * @param sourceImageWidth  The source image width.
     * @param sourceImageHeight The source image height.
     * @param srcIndex          The TileIndex of the first source tile
     * @param dataBuffers       Source tile data buffers.
     * @param Cr                The real part of the mean scattering matrix.
     * @param Ci                The imaginary part of the mean scattering matrix.
     */
    public static void getMeanCovarianceMatrix(
            final int x, final int y, final int halfWindowSizeX, final int halfWindowSizeY,
            final int sourceImageWidth, final int sourceImageHeight,
            final PolBandUtils.MATRIX sourceProductType, final TileIndex srcIndex, final ProductData[] dataBuffers,
            final double[][] Cr, final double[][] Ci) {


        final double[][] tempCr = new double[3][3];
        final double[][] tempCi = new double[3][3];


        final int xSt = FastMath.max(x - halfWindowSizeX, 0);
        final int xEd = FastMath.min(x + halfWindowSizeX, sourceImageWidth - 1);
        final int ySt = FastMath.max(y - halfWindowSizeY, 0);
        final int yEd = FastMath.min(y + halfWindowSizeY, sourceImageHeight - 1);
        final int num = (yEd - ySt + 1) * (xEd - xSt + 1);
        
        final Matrix CrMat = new Matrix(3, 3);
        final Matrix CiMat = new Matrix(3, 3);

        if (sourceProductType == PolBandUtils.MATRIX.C3) {

            /* Iterate over the 5x5 window and add the results togetehr in CrMat and CiMat */
            for (int yy = ySt; yy <= yEd; ++yy) {
                srcIndex.calculateStride(yy);
                for (int xx = xSt; xx <= xEd; ++xx) {
                    getCovarianceMatrixC3(srcIndex.getIndex(xx), dataBuffers, tempCr, tempCi);
                    
                    CrMat.plusEquals(new Matrix(tempCr));
                    CiMat.plusEquals(new Matrix(tempCi));
                    
                    /*System.out.println("yy : "+yy);
                    System.out.println("xx : " +xx);
                    System.out.println("CrMat : ");
                    CrMat.print(10,5);
                    System.out.println("CiMat : ");
                    CiMat.print(10,5);*/
                }
            }
        }

        /* Divide by the number of pixels used in average (the window is not complete for edge pixels)*/
        CrMat.timesEquals(1.0 / num);
        CiMat.timesEquals(1.0 / num);
        
        /*System.out.println("CrMat : ");
        CrMat.print(10, 5);
        System.out.println("Cr elements :");*/
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                //System.out.println("CrMat.get("+i+","+j+") : "+CrMat.get(i, j));
                Cr[i][j] = CrMat.get(i, j);
                Ci[i][j] = CiMat.get(i, j);
            }
        }
    }
    
    public static void getMeanCovarianceMatrixC2(
            final int x, final int y, final int halfWindowSizeX, final int halfWindowSizeY,
            final int sourceImageWidth, final int sourceImageHeight,
            final PolBandUtils.MATRIX sourceProductType, final TileIndex srcIndex, final ProductData[] dataBuffers,
            final double[][] Cr, final double[][] Ci) {


        final double[][] tempCr = new double[2][2];
        final double[][] tempCi = new double[2][2];


        final int xSt = FastMath.max(x - halfWindowSizeX, 0);
        final int xEd = FastMath.min(x + halfWindowSizeX, sourceImageWidth - 1);
        final int ySt = FastMath.max(y - halfWindowSizeY, 0);
        final int yEd = FastMath.min(y + halfWindowSizeY, sourceImageHeight - 1);
        final int num = (yEd - ySt + 1) * (xEd - xSt + 1);
        
        final Matrix CrMat = new Matrix(2, 2);
        final Matrix CiMat = new Matrix(2, 2);
        
        if (sourceProductType == PolBandUtils.MATRIX.C2) {
            
            /* Iterate over the 5x5 window and add the results together in CrMat and CiMat */
            for (int yy = ySt; yy <= yEd; ++yy) {
                srcIndex.calculateStride(yy);
                for (int xx = xSt; xx <= xEd; ++xx) {
                    getCovarianceMatrixC2(srcIndex.getIndex(xx), dataBuffers, tempCr, tempCi);
                    CrMat.plusEquals(new Matrix(tempCr));
                    CiMat.plusEquals(new Matrix(tempCi));
                    
                    /*System.out.println("yy : "+yy);
                    System.out.println("xx : " +xx);
                    System.out.println("CrMat : ");
                    CrMat.print(10,5);
                    System.out.println("CiMat : ");
                    CiMat.print(10,5);*/
                }
            }
        }

        /* Divide by the number of pixels used in average (the window is not complete for edge pixels)*/
        CrMat.timesEquals(1.0 / num);
        CiMat.timesEquals(1.0 / num);
        
        /*System.out.println("CrMat : ");
        CrMat.print(10, 5);
        System.out.println("Cr elements :");*/
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                //System.out.println("CrMat.get("+i+","+j+") : "+CrMat.get(i, j));
                Cr[i][j] = CrMat.get(i, j);
                Ci[i][j] = CiMat.get(i, j);
            }
        }
        
    }
    
     /**
     * Get covariance matrix C3 for given pixel.
     *
     * @param index       X,Y coordinate of the given pixel
     * @param dataBuffers Source tiles dataBuffers for all 9 source bands
     * @param Cr          Real part of the covariance matrix
     * @param Ci          Imaginary part of the covariance matrix
     */
    public static void getCovarianceMatrixC3(final int index, final ProductData[] dataBuffers,
                                             final double[][] Cr, final double[][] Ci) {

        Cr[0][0] = dataBuffers[0].getElemDoubleAt(index); // C11 - real
        Ci[0][0] = 0.0;                                   // C11 - imag

        Cr[0][1] = dataBuffers[1].getElemDoubleAt(index); // C12 - real
        Ci[0][1] = dataBuffers[2].getElemDoubleAt(index); // C12 - imag

        Cr[0][2] = dataBuffers[3].getElemDoubleAt(index); // C13 - real
        Ci[0][2] = dataBuffers[4].getElemDoubleAt(index); // C13 - imag

        Cr[1][1] = dataBuffers[5].getElemDoubleAt(index); // C22 - real
        Ci[1][1] = 0.0;                                   // C22 - imag

        Cr[1][2] = dataBuffers[6].getElemDoubleAt(index); // C23 - real
        Ci[1][2] = dataBuffers[7].getElemDoubleAt(index); // C23 - imag

        Cr[2][2] = dataBuffers[8].getElemDoubleAt(index); // C33 - real
        Ci[2][2] = 0.0;                                   // C33 - imag

        Cr[1][0] = Cr[0][1];
        Ci[1][0] = -Ci[0][1];
        Cr[2][0] = Cr[0][2];
        Ci[2][0] = -Ci[0][2];
        Cr[2][1] = Cr[1][2];
        Ci[2][1] = -Ci[1][2];            
    }
    
/**
     * Get covariance matrix C2 for given pixel.
     *
     * @param index       X,Y coordinate of the given pixel
     * @param dataBuffers Source tiles dataBuffers for all 4 source bands
     * @param Cr          Real part of the covariance matrix
     * @param Ci          Imaginary part of the covariance matrix
     */
    public static void getCovarianceMatrixC2(final int index, final ProductData[] dataBuffers,
                                             final double[][] Cr, final double[][] Ci) {

        Cr[0][0] = dataBuffers[0].getElemDoubleAt(index); // C11 - real
        Ci[0][0] = 0.0;                                   // C11 - imag

        Cr[0][1] = dataBuffers[1].getElemDoubleAt(index); // C12 - real
        Ci[0][1] = dataBuffers[2].getElemDoubleAt(index); // C12 - imag

        Cr[1][1] = dataBuffers[3].getElemDoubleAt(index); // C22 - real
        Ci[1][1] = 0.0;                                   // C22 - imag


        Cr[1][0] = Cr[0][1];
        Ci[1][0] = -Ci[0][1];
          
    }    
}