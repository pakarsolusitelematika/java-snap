/*
 * Image writing utiles (quicklooks)
 * - getColorsHEX : returns a String[] of HEX codes for classif colors  (String[])
 * - createRendering : returns a BufferedImageRendering for the multilayered quicklook (BufferedImageRendering
 * - MyLayerContext : returns Layer Context from input product and layer (LayerContext)
 */
package com.batchprocessing.java.snap;

import com.bc.ceres.glayer.Layer;
import com.bc.ceres.glayer.LayerContext;
import com.bc.ceres.glevel.MultiLevelModel;
import com.bc.ceres.grender.Viewport;
import com.bc.ceres.grender.support.BufferedImageRendering;
import com.bc.ceres.grender.support.DefaultViewport;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import org.esa.snap.core.datamodel.Product;

/**
 *
 * @author Marco Peters
 */
public class ImageUtils {

    public static String[] getColorsHEX(String classif) {
        String[] colors = null;
        if (classif.equals("_mlc")) {
            colors = new String[3];
            colors[0] = String.format("#%02x%02x%02x",98,46,179); // purple
            colors[1] = String.format("#%02x%02x%02x",186,166,187); //lightPink
            colors[2] = String.format("#%02x%02x%02x",8,56,151);      //darkBlue           
        } else {
            System.out.println("User warning : classif not included in getColorsHEX");
        }
        return colors;
    }
    
    public static BufferedImageRendering createRendering(BufferedImage bufferedImage, MultiLevelModel multiLevelModel) {
        AffineTransform m2iTransform = multiLevelModel.getModelToImageTransform(0);
        final Viewport vp2 = new DefaultViewport(new Rectangle(bufferedImage.getWidth(), bufferedImage.getHeight()),
                m2iTransform.getDeterminant() > 0.0);
        vp2.zoom(multiLevelModel.getModelBounds());

        final BufferedImageRendering imageRendering = new BufferedImageRendering(bufferedImage, vp2);
        // because image to model transform is stored with the exported image we have to invert
        // image to view transformation
        final AffineTransform v2mTransform = vp2.getViewToModelTransform();
        v2mTransform.preConcatenate(m2iTransform);
        final AffineTransform v2iTransform = new AffineTransform(v2mTransform);

        final Graphics2D graphics2D = imageRendering.getGraphics();
        v2iTransform.concatenate(graphics2D.getTransform());
        graphics2D.setTransform(v2iTransform);
        return imageRendering;
    }

    // Written by Marco Peters as a SNAP example 
    public static class MyLayerContext implements LayerContext {

        private final Product product;
        private final Layer rootLayer;

        public MyLayerContext(Product product, Layer rootLayer) {
            this.product = product;
            this.rootLayer = rootLayer;
        }

        @Override
        public Object getCoordinateReferenceSystem() {
            return product.getSceneGeoCoding().getMapCRS();
        }

        @Override
        public Layer getRootLayer() {
            return rootLayer;
        }
    }
}
