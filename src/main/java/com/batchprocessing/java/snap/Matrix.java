/*
 * Convert scattering matrix to polarimetric matrix
 * - C2: create C2 product (Product)
 * - C3: create C3 product (Product)
 */
package com.batchprocessing.java.snap;

import java.io.File;
import java.util.HashMap;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.gpf.GPF;

/**
 *
 * @author dufobeso
 * S. Dufour-Beauséjour
 * PhD student at INRS-ETE
 * Québec, Canada
 * s.dufour.beausejour@gmail.com
 */
public class Matrix {

    /* Convert to covariance C2 matrix */
    public static Product C2(Product product, File imageFile){
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();
        
        /* Operator parameters*/
        HashMap parameters = new HashMap();
        parameters.put("matrix","C2");
        
        
        System.out.println("Converting to polarimetric C2 covariance matrix...");
        Product outProduct = GPF.createProduct("Polarimetric-Matrices", parameters, product);        
        System.out.println("Done.");
        
        /* Update product description and name */
        String date = Utils.getTimeStamp();
        
        outProduct.setDescription(product.getDescription()+"Product converted to C2 matrix (" + date + ").\n");
        outProduct.setName(product.getName()+"_C2");
        return outProduct;             
    }    
    
    /* Convert to covariance C3 matrix */
    public static Product C3(Product product, File imageFile){
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();
        
        /* Operator parameters*/
        HashMap parameters = new HashMap();
        parameters.put("matrix","C3");
        
        
        System.out.println("Converting to polarimetric C3 covariance matrix...");
        Product outProduct = GPF.createProduct("Polarimetric-Matrices", parameters, product);        
        System.out.println("Done.");
        
        /* Update product description and name */
        String date = Utils.getTimeStamp();
        
        outProduct.setDescription(product.getDescription()+"Product converted to C3 matrix (" + date + ").\n");
        outProduct.setName(product.getName()+"_C3");
        return outProduct;             
    }    
}
